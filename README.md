# NewParticleFormation

## Name
New Particle Formation in Amazonas

## Description
This project includes all data and scripts used to prepare the manuscript - Frequent nanoparticle bursts in the Amazon
rainforest

## Installation
This project uses only Python3 as basic system and the following libraries:
- xarray
- pandas
- numpy
- matplotlib
- datetime
- scipy
- warnings
- math
- csv
- sys
- itertools
- seaborn
- pytz


## Usage
The use of the scripts to produce the results is detailed in the  Description session.
## Support
Any doubt, please mail - l.machado@mpic.de

## Authors and acknowledgment
The scripts were develloped by Luiz A. T. Machado expect : Gabriela Unfer develloped the scripts for figure1 and figure_sup2.3.py and Stefanie Hildmann developped figure_sup2.10 using Origin.

## License
Open source scripts, If you want to use the data for other projects, please contact me.

## Description

Readme file - Details
#
The programs are in the directory SCRIPT and the SCRIPT/make_codes
#
All basic data are on the directory data_fonte
#
All compiled data are on the directory data_composite
#
All figures are stored in the directory xfigure_vf_npf
#
To produce manuscript figures, the codes need the data stored in the data_fonte directory and the data_composite. To create the data composite, it is required to run the scripts in the directory SCRIPT/make_codes: 
#
The codes on the SCRIPT directory can be run only using python3 name.py
#
The codes on the make_codes directory can be run using the following command – Python3 make_name_code.py var_12.
Var_12 is the option for the composite taken for all cases for this specific application is the rainfall, with the threshold of 0.5 mm.hr-1.

The codes are:
#
make_geral_vf_all_fig4.py
#
make_geral_vf_meteonew_sup.py
#
make_geral_vf_NO_NO2_sup.py
#
make_geral_vf_O3_fig3.py
#
make_geral_vf_part_fig2.py
#
make_geral_vf_roli.py
#
make_geral_vf_tof_sup.py – need to choose isoprene or monoterpenes. The comments are on the code for a choice definition.
#
make_vf_O3.py - need to choose the rainfall value - 6.0, 4.5, 3.0, 1.5 and 0.5 mm.hr-1. The comments are on the code for a choice definition.

#
These codes produce the composite – related to the selected variable. 
In our case, we use var_12, which corresponds to the rainfall. The others var_# are those variables
composed in the -5 hours to +5 hours of the maximum rain rate.
All these codes store the variable as: (example make_geral_vf_NO_NO2_sup.py)
#

dfg['var_1']=dfc1['NO_0.05']
#
dfg['var_2']=dfc1['NO_0.5']
#
dfg['var_3']=dfc1['NO_4']
#
dfg['var_4']=dfc1['NO_12']
#
dfg['var_5']=dfc1['NO_24']
#
dfg['var_6']=dfc1['NO_38']
#
dfg['var_7']=dfc1['NO_53']
#
dfg['var_8']=dfc1['NO_79']
#
dfg['var_9']=dfc1['NO2_0.05']
#
dfg['var_10']=dfc1['NO2_0.5']
#
dfg['var_11']=dfc1['NO2_4']
#
#
dfg['var_12']=df_tir['Precipitation']
#
#
dfg['var_13']=dfc1['NO2_12']
#
dfg['var_14']=dfc1['NO2_24']
#
dfg['var_15']=dfc1['NO2_38']
#
dfg['var_16']=dfc1['NO2_53']
#
dfg['var_17']=dfc1['NO2_79']
#
dfg['var_18']=df_tof['monoterpenes_1']
#

The var_12 is always the rain rate – however, each code has different positions 
for each variable – so when using the codes to generate the figures, 
please look at the make-code to figure with variables were defined in var_1, var_2, var_3, etc.
You can run any of the codes in SCRIPT with these two data directories (they are already on the 
data_composite directory).
#
The codes are named with the number of the figure. Therefore, it is straightforward to 
figure out what the code produces (only Figure 5 has no script because it is a schematic Figure). Figure 11 could be run for low and high ozone concentrations. The comments are on the code for a choice definition.
There is no  specific script to produce Figure_sup2.10, the data is in excel (Excel_dataprocessing.xlx) and figure was produced using Origin  (no specific codes is needed ). In the directory Xfigure_vf_npf you can find the Origin file.




