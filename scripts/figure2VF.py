import xarray as xr
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator,MultipleLocator
import matplotlib as mpl
# #############################################################
# #
# #                              
# ##############################################################
color1='red'
color2='blue'
color3='gray'
color4='dimgray'
color5 = 'black'
#################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_part_wet_var_12.nc')
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
##################################################################################
#############################################################################################
print(df_glm_q25)
dff=df_glm_q25
dff.index=df_glm_q25['tstep']
ix=0
ixx=0
for i in range(len(dff)):
    if(dff.index[i]==-2): 
        ixx=ixx+1
for i in range(len(dff)):
    if(dff.index[i]==-2 and math.isnan(dff['var_8'].iloc[i])): 
        ix=ix+1
print('total values geral =  ',ixx)
print('total values on composite=  ',ixx-ix)
#
################################################################################
##
#                               Part
######################################################################################################
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix
########################################################################################
#
fig,ax=plt.subplots(figsize=(10,8))
ax2=ax.twinx()
ax3=ax.twinx()
ax3.spines['right'].set_position(('outward', 80))
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_major_locator(MultipleLocator(1))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#

ax.spines['left'].set_color('black')
ax2.spines['right'].set_color('red')
ax3.spines['right'].set_color('blue')
ax.tick_params(axis='both', which='major', labelsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax3.tick_params(axis='both', which='major', labelsize=15)
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax2.spines['right'].set_linewidth(3)
ax3.spines['right'].set_linewidth(3)
#
#
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig2a.jpg'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
ax2.xaxis.set_minor_locator(  AutoMinorLocator(5))
ax3.xaxis.set_minor_locator(  AutoMinorLocator(5))
#

plot1=sns.lineplot(x='tstep',y='var_3',data=df_glm_q25,ci=95,ax=ax,color=color3,lw=2, marker=('o'),label='UFP40-60 H=60 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_4',data=df_glm_q25,ci=95,ax=ax,color=color4,lw=2, marker=('*'),label='UFP60-80 H=60 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_5',data=df_glm_q25,ci=95,ax=ax,color=color5,lw=2, marker=('x'),label='UFP80-100 H=60 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_1',data=df_glm_q25,ci=95,ax=ax2,color=color1,lw=2, marker=('o'),label='UFP<20 H=60 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_2',data=df_glm_q25,ci=95,ax=ax3,color=color2,lw=2, marker=('o'),label='UFP20-40 H=60 m',legend=False, estimator=np.median)

#
h1, l1 = ax.get_legend_handles_labels()
h2, l2 = ax2.get_legend_handles_labels()
h3, l3 = ax3.get_legend_handles_labels()
#
plt.legend(h1+h2+h3, l1+l2+l3, loc=3, fontsize=15)
#plt.title('Particle Size Distibution - Clean Season - Feb-May')


ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
#
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=15)
ax.set_ylabel(" UFP 40-100 nm - Num. Con.  [$cm^{-3}$] ",fontsize=15)
ax2.set_ylabel(" UFP<20 nm - Num. Con.  [$cm^{-3}$] ",color=color1,fontsize=15)
ax3.set_ylabel(" UFP 20-40 nm-  Num. Con.  [$cm^{-3}$] ", color=color2,fontsize=15)
#
ax.tick_params(axis='both', which='major', labelsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax3.tick_params(axis='both', which='major', labelsize=15)
#
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_major_locator(MultipleLocator(1))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
ax.spines['left'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
#
ax.set_ylim(90,260)
ax2.set_ylim(0,28)                 ############## MEDIAN ##################
ax3.set_ylim(20,140)    
#
#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
#
#                         325m
#
fig,ax=plt.subplots(figsize=(10,8))
ax2=ax.twinx()
ax3=ax.twinx()
ax3.spines['right'].set_position(('outward', 80))
#
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig2b.jpg'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
ax2.xaxis.set_minor_locator(  AutoMinorLocator(5))
ax3.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
plot1=sns.lineplot(x='tstep',y='var_8',data=df_glm_q25,ci=95,ax=ax,color=color3,lw=2, marker=('o'),label='UFP40-60 H=325 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_9',data=df_glm_q25,ci=95,ax=ax,color=color4,lw=2, marker=('*'),label='UFP60-80 H=325 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_10',data=df_glm_q25,ci=95,ax=ax,color=color5,lw=2, marker=('x'),label='UFP80-100 H=325 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_6',data=df_glm_q25,ci=95,ax=ax2,color=color1,lw=2, marker=('o'),label='UFP<20 H=325 m',legend=False, estimator=np.median)
plot1=sns.lineplot(x='tstep',y='var_7',data=df_glm_q25,ci=95,ax=ax3,color=color2,lw=2, marker=('o'),label='UFP20-40 H=325 m',legend=False, estimator=np.median)
#
h1, l1 = ax.get_legend_handles_labels()
h2, l2 = ax2.get_legend_handles_labels()
h3, l3 = ax3.get_legend_handles_labels()
#
plt.legend(h1+h2+h3, l1+l2+l3, loc=1, fontsize=15)
#plt.title('Particle Size Distibution - Clean Season - Feb-May')
ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
#
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=15)
ax.set_ylabel(" UFP 40-100 nm - Num. Con.  [$cm^{-3}$] ",fontsize=15)
ax2.set_ylabel(" UFP<20 nm - Num. Con.  [$cm^{-3}$] ",color=color1,fontsize=15)
ax3.set_ylabel(" UFP 20-40 nm-  Num. Con.  [$cm^{-3}$] ", color=color2,fontsize=15)
#

ax.tick_params(axis='both', which='major', labelsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax3.tick_params(axis='both', which='major', labelsize=15)
#
ax.spines['left'].set_color('black')
ax2.spines['right'].set_color('red')
ax3.spines['right'].set_color('blue')
ax.tick_params(axis='both', which='major', labelsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax3.tick_params(axis='both', which='major', labelsize=15)
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax2.spines['right'].set_linewidth(3)
ax3.spines['right'].set_linewidth(3)
#
#ax.set_ylim(150,325)
#ax2.set_ylim(5,45)
#ax3.set_ylim(45,180)



ax.set_ylim(90,260)
ax2.set_ylim(0,28)                 ############## MEDIAN ##################
ax3.set_ylim(20,140)
#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
