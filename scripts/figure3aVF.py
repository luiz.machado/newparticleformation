import xarray as xr
import math
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator
import matplotlib as mpl
from scipy.interpolate import interp1d
from scipy.ndimage.filters import gaussian_filter
from matplotlib.ticker import MaxNLocator
from matplotlib.ticker import (MultipleLocator,
                               FormatStrFormatter,
                               AutoMinorLocator)
#############################################################################
#######################  O3        #################################
#################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_wet_var_12.nc')
df= pd.DataFrame()
df=df1.to_dataframe()
#############################################################################################
print(df)
stix=[]
for i,ia in enumerate(df['case']):
    stix.append(i)
df.index=stix
print(df)
#
###########################################################################3
fig,ax=plt.subplots(figsize=(10,8))
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig3a.jpg'
#
color1='black'
color2='grey'
color3='brown'
color4 = 'red'
color5= 'green'
color6='mediumspringgreen'
color7 = 'deepskyblue'
color8='blue'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
plot1=sns.lineplot(x='tstep',y='var_1',data=df,ci=95,ax=ax,color=color1,lw=3, marker=('o'),label='H=0.05 m')
plot1=sns.lineplot(x='tstep',y='var_2',data=df,ci=95,ax=ax,color=color2,lw=3, marker=('*'),label='H=0.5 m')
plot1=sns.lineplot(x='tstep',y='var_3',data=df,ci=95,ax=ax,color=color3,lw=3, marker=('v'),label='H=4 m')
plot1=sns.lineplot(x='tstep',y='var_4',data=df,ci=95,ax=ax,color=color4,lw=3, marker=('x'),label='H=12 m')
plot1=sns.lineplot(x='tstep',y='var_5',data=df,ci=95,ax=ax,color=color5,lw=3, marker=('d'),label='H=24 m')
plot1=sns.lineplot(x='tstep',y='var_6',data=df,ci=95,ax=ax,color=color6,lw=3, marker=('p'),label='H=38 m')
plot1=sns.lineplot(x='tstep',y='var_7',data=df,ci=95,ax=ax,color=color7,lw=3, marker=('h'),label='H=53 m')
plot1=sns.lineplot(x='tstep',y='var_8',data=df,ci=95,ax=ax,color=color8,lw=3, marker=('D'),label='H=79 m')
#
plt.legend(ncol=2,loc=2, fontsize=15)
ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
#
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate ", fontsize=15)
ax.set_ylabel("  O3 Concentration (ppb)", fontsize=15)
ax.tick_params(axis='both', which='major', labelsize=15)
ax.spines['left'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()