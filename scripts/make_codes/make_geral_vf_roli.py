import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime, timedelta
import warnings 
var_name=sys.argv[1]
#
#################################################################################
tof = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/roli_nano.nc')
df_tof1=tof.to_dataframe()
df_tof1.index=pd.DatetimeIndex(df_tof1.index)
df_tof1['data']=df_tof1.index
df_tof1.index.names=['time']
df_tof=df_tof1.resample('30Min').mean()
#
####################################### OPEN AND SEASON CLASSIFY #################################################################
df2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/picarro_filtered.nc')
dfc=df2.to_dataframe()
#
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/lycor_filtered.nc')
dfc1=df1.to_dataframe()
#######################################################
#            NUCL. Particles 60
####################################################
dt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine1.nc')
d1= pd.DataFrame()
d1=dt1.to_dataframe()
dt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine2.nc')
d2= pd.DataFrame()
d2=dt2.to_dataframe()
dt3= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine3.nc')
d3= pd.DataFrame()
d3=dt3.to_dataframe()
dt4= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine4.nc')
d4= pd.DataFrame()
d4=dt4.to_dataframe()
dt5= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine5.nc')
d5= pd.DataFrame()
d5=dt5.to_dataframe()
d1.index.names=['time']
d2.index.names=['time']
d3.index.names=['time']
d4.index.names=['time']
d5.index.names=['time']
########################################################
#           325 m
#
#####################################################
dxt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine1.nc')
dx1= pd.DataFrame()
dx1=dxt1.to_dataframe()
dxt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine2.nc')
dx2= pd.DataFrame()
dx2=dxt2.to_dataframe()
dxt3= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine3.nc')
dx3= pd.DataFrame()
dx3=dxt3.to_dataframe()
dxt4= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine4.nc')
dx4= pd.DataFrame()
dx4=dxt4.to_dataframe()
dxt5= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine5.nc')
dx5= pd.DataFrame()
dx5=dxt5.to_dataframe()
dx1.index.names=['time']
dx2.index.names=['time']
dx3.index.names=['time']
dx4.index.names=['time']
dx5.index.names=['time']
########################################################
####################################################################################################################
# ABRE Tir
#
###################################################################################################################
################################################################################
##############################################################################
ds0 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/rain325_2023.nc')
tir0 = ds0.to_dataframe()[['Precipitation_325']]
df_tir0=pd.DataFrame()
df_tir0=tir0.resample('30Min').mean()
df_tir0.index=df_tir0.index+timedelta(hours=4)
df_tir0.index.names=['time']
######################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/rain81_2023.nc')
tir1 = ds1.to_dataframe()[['Precipitation']]
df_tir1=pd.DataFrame()
df_tir1=tir1.resample('30Min').mean()
df_tir1.index.names=['time']
#########################################################################################
ds2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/gpm2023.nc')
tir2 = ds2.to_dataframe()[['Precipitation']]
df_tir2=pd.DataFrame()
df_tir2=tir2.resample('30Min').mean()
df_tir2.index.names=['time']
##########################################################################################
################################################################################
df_tir=pd.DataFrame()
df_tir.index=pd.DatetimeIndex(df_tir0.index)
df_tir['Precipitation_325']=df_tir0['Precipitation_325']
df_tir['Precipitation_81']=df_tir1['Precipitation']
df_tir['Precipitation_gpm']=df_tir2['Precipitation']
df_tir['Precipitation']=df_tir[['Precipitation_325','Precipitation_81','Precipitation_gpm']].mean(axis=1,skipna=True)
###############################################################################
################################## OPEN RADIATION #####################################################
ds2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/meteodata.nc')
tr = ds2.to_dataframe()
tr=tr[tr['Solar_inc_Wm2']<=1600]
tr=tr.resample('30Min').mean()
tr.index.names=['time']
########################################################
#              Build the dfg Array                     #
########################################################
date_rng = pd.date_range(start='2023-01-01 00:00:00',end='2023-10-30 00:00:00',freq='30T')
dfg =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18','time'])
#######################################################################################################
#   load alll valeus on the dfg array
#####################################################################
dfg['var_1']=d1['diam1']
dfg['var_2']=d2['diam2']
dfg['var_3']=d3['diam3']
dfg['var_4']=d4['diam4']
dfg['var_5']=d5['diam5']
dfg['var_6']=df_tof['sub20_40']
dfg['var_7']=df_tof['sub20_280']
dfg['var_8']=df_tof['sub40_40']
dfg['var_9']=df_tof['sub40_280']
dfg['var_10']=df_tof['sub60_40']
dfg['var_11']=df_tof['sub60_280']
dfg['var_12']=df_tir['Precipitation']
dfg['var_13']=tr['Solar_inc_Wm2']
dfg['var_14']=df_tof['small20']
dfg['var_15']=df_tof['small40']
dfg['var_16']=df_tof['small60']
dfg['var_17']=0.0
dfg['var_18']=0.0
dfg['time']=dfg.index
################################################################
#           Select Range of Analysis
################################################################
dfg=dfg.loc['2023-01-01 00:00:00':'2023-10-30 00:00:00']
##################################################################
#        Define    filter outlier - eliminate  larger values the 0.0001 - keep 99.999% - xlim_out and select the larger values to be evaluated - quantile Xx
######################################################
# rain rate limit
###############################
xlim=0.5
###########################################################################
############# split in dry and wet
####################################################################################
dfg_wet=dfg
###########################################################################
#
#                          filtered data

###############################found the First value , larger than xlim in the 10 hours slice  #################################################
###########################################################################

###### GET ALL MOMENTS ############################################################################
stix = []
stix_first_moment = []  # list para armazenamento dos primeiros momentos
stix_last_moment = []   # list para armazenamento dos ultimos momentos  
stix_maximo = []        # list para armazenamento dos maximos valores
groups_id=dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).groups.keys()

for gid in list(groups_id):

    stix = [] # list para armazenamento temporario de indices
    stiv = [] # list para armazenamento temporario de valores

    dfg_group = dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).get_group(gid)

    max_value = 0
    max_id = 0
    for row_id, row_value in dfg_group[var_name].iteritems():
        if(str(row_id)!=str(pd.NaT)):
            if(row_value >= xlim):
                stix.append(row_id)
                if (row_value != np.nan) & (row_value > max_value):
                    max_value = row_value
                    max_id = row_id

    # Em caso de NaT não armazena no dataframe
    if len(stix) != 0:
        stix_first_moment.append(pd.DataFrame(stix).head(1).values.squeeze())
        stix_last_moment.append(pd.DataFrame(stix).tail(1).values.squeeze())
        stix_maximo.append(max_id)
#
stix_last_moment=np.array(stix_last_moment)
stix_last_moment.astype(datetime)
#
# Cria dataframe com os momentos
df_moments = pd.DataFrame({'first_moment':stix_first_moment,'max_moment':stix_maximo,'last_moment':stix_last_moment})
#################################################################################################
#
momento = 'max_moment'
sti1 = pd.DataFrame(df_moments[momento].values, columns=['time'])
#
###########################33
#  eliminate neighboors time (lasts of 10 hours and firsts of the next 10 hours)
##############################
sti_shift=sti1.shift(1)
dif=abs(sti1-sti_shift)
sti1['dif']=dif
#  mask with 180 minutes delta:
m = (sti1['dif'] >= pd.Timedelta('0 days 00:180:00')) 
sti1=sti1[m]
sti1=sti1.drop(columns=['dif'])
#################################################################################
sti=sti1
with open("output.txt", "w") as output_file:
    for i, a in enumerate(sti.values):  

        output_file.write(str(a)+ "\n")
##################################################################################                  
#  
###################################################################################################
#####################################################################################
#
#                     Selected serie - add to df the one you selected
#
###########################################################################
############ filter  Wet
###########################################################################
################################################
#
sti=sti[(sti['time'].dt.month>=2) & (sti['time'].dt.month<=5)] ####    WET 
#
#################################################################################
#
st=sti - timedelta(hours=5)
#
end=sti + timedelta(hours=5)
##################################################################################                  
#  
###################################################################################################
#####################################################################################
#
#                     Selected serie - add to df the one you selected
#
#####################################################################################
case_fail = set()
#
df = pd.DataFrame([],columns=['case','tstep','time','var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18'])

for i, a in enumerate(sti.values):       
#
        ind=list(range(-10,11,1))
#
        for ti, tindex in enumerate(dfg_wet['time'].loc[st.iloc[i].values[0]:end.iloc[i].values[0]]):
                       
#
        # A instrução TRY foi utilizada abaixo porque o range de tempo no dataframe smpc_d1_325 é menor e gera um erro quando o tempo não é encontrado
           
            try:
                dt = {'case':i, 'tstep':ind[ti], 'time':tindex,
                'var_1':dfg_wet['var_1'].loc[tindex],
                'var_2':dfg_wet['var_2'].loc[tindex],
                'var_3':dfg_wet['var_3'].loc[tindex],
                'var_4':dfg_wet['var_4'].loc[tindex],
                'var_5':dfg_wet['var_5'].loc[tindex],
                'var_6':dfg_wet['var_6'].loc[tindex],
                'var_7':dfg_wet['var_7'].loc[tindex],
                'var_8':dfg_wet['var_8'].loc[tindex],
                'var_9':dfg_wet['var_9'].loc[tindex],
                'var_10':dfg_wet['var_10'].loc[tindex],
                'var_11':dfg_wet['var_11'].loc[tindex],
                'var_12':dfg_wet['var_12'].loc[tindex],
                'var_13':dfg_wet['var_13'].loc[tindex],
                'var_14':dfg_wet['var_14'].loc[tindex],
                'var_15':dfg_wet['var_15'].loc[tindex],
                'var_16':dfg_wet['var_16'].loc[tindex],
                'var_17':dfg_wet['var_17'].loc[tindex],
                'var_18':dfg_wet['var_18'].loc[tindex]}
                dfi=pd.DataFrame(dt,index=[(i)])
                df=pd.concat([df,dfi])
                
        # A instrução EXCEPT apenas guarda os casos em que ocorreram problemas
            except:
                case_fail.add(i)
##################################################################################################
df['tstep']=df['tstep']*0.5
#########################################################################################################################
#
#                  Write the data
#
#########################################################################################################################
saidanc = df.to_xarray()
saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_roli_wet_'+str(var_name)+'.nc')
print('done wet-day')


