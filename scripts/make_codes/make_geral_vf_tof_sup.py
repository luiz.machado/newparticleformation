import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import seaborn as sns
import warnings 
warnings.filterwarnings("ignore")
# opening database
#
#################### OPEN AND SEASON CLASSIFY #################################################################
#######################################################################################################
#
df = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/tof_profile.nc')
#
voc=df.to_dataframe()
#
voc['monoterpenes_1']=voc['monoterpenes_1'].where(voc['monoterpenes_1']<=voc['monoterpenes_1'].quantile(0.999),np.nan)
voc['monoterpenes_2']=voc['monoterpenes_2'].where(voc['monoterpenes_2']<=voc['monoterpenes_2'].quantile(0.999),np.nan)
voc['monoterpenes_3']=voc['monoterpenes_3'].where(voc['monoterpenes_3']<=voc['monoterpenes_3'].quantile(0.999),np.nan)
#
voc['isoprene_1']=voc['isoprene_1'].where(voc['isoprene_1']<=voc['isoprene_1'].quantile(0.999),np.nan)
voc['isoprene_2']=voc['isoprene_2'].where(voc['isoprene_2']<=voc['isoprene_2'].quantile(0.999),np.nan)
voc['isoprene_3']=voc['isoprene_3'].where(voc['isoprene_3']<=voc['isoprene_3'].quantile(0.999),np.nan)
#
voc=voc.resample('30Min').mean()
#
voc['monoterpenes_1']=voc['monoterpenes_1'].where(voc['monoterpenes_1']!=0,np.nan)
voc['monoterpenes_2']=voc['monoterpenes_2'].where(voc['monoterpenes_2']!=0,np.nan)
voc['monoterpenes_3']=voc['monoterpenes_3'].where(voc['monoterpenes_3']!=0,np.nan)
#
voc['R_1']=voc['isoprene_1']/voc['monoterpenes_1']
voc['R_2']=voc['isoprene_2']/voc['monoterpenes_2']
voc['R_3']=voc['isoprene_3']/voc['monoterpenes_3']
#
print(voc)
############################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/teta.nc')
tir = ds1.to_dataframe()[['Precipitation']]
#tir=tir[tir['Precipitation']>0.0]
df_tir=pd.DataFrame()
df_tir=tir.resample('30Min').max()
df_tir.index.names=['time']
########################################################
#              Build the dfg Array                     #
########################################################
date_rng = pd.date_range(start='2018-01-01 00:00:00',end='2019-12-20 23:30:00',freq='30T')
dfg =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_12','time'])
#######################################################################################################
#   load alll valeus on the dfg array
#####################################################################
#
#chose isoprene or monoterpene - need to change also the output
#
#################################################################
#dfg['var_1']=voc['monoterpenes_1']
#dfg['var_2']=voc['monoterpenes_2']
#dfg['var_3']=voc['monoterpenes_3']
#
dfg['var_1']=voc['isoprene_1']
dfg['var_2']=voc['isoprene_2']
dfg['var_3']=voc['isoprene_3']
#
dfg['var_12']=df_tir['Precipitation']
dfg['time']=dfg.index
################################################################
#           Select Range of Analysis
################################################################
dfg=dfg.loc['2018-01-01 00:00:00':'2019-12-31 00:00:00']
##################################################################
#        Define    filter outlier - eliminate  larger values the 0.0001 - keep 99.999% - xlim_out and select the larger values to be evaluated - quantile Xx
######################################################
# rain rate limit
###################
xlim=0.5
print('limit--> = ',xlim,'var_12')
###########################################################################
############# split in dry and wet
####################################################################################
dfg_wet=dfg
###########################################################################
#
#                          filtered data

###############################found the First value , larger than xlim in the 10 hours slice  #################################################
###########################################################################

###### GET ALL MOMENTS ############################################################################
stix = []
stix_first_moment = []  # list para armazenamento dos primeiros momentos
stix_last_moment = []   # list para armazenamento dos ultimos momentos  
stix_maximo = []        # list para armazenamento dos maximos valores
groups_id=dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).groups.keys()

for gid in list(groups_id):

    stix = [] # list para armazenamento temporario de indices
    stiv = [] # list para armazenamento temporario de valores

    dfg_group = dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).get_group(gid)

    max_value = 0
    max_id = 0
    for row_id, row_value in dfg_group['var_12'].iteritems():
        if(str(row_id)!=str(pd.NaT)):
            if(row_value >= xlim):
                stix.append(row_id)
                if (row_value != np.nan) & (row_value > max_value):
                    max_value = row_value
                    max_id = row_id

    # Em caso de NaT não armazena no dataframe
    if len(stix) != 0:
        stix_first_moment.append(pd.DataFrame(stix).head(1).values.squeeze())
        stix_last_moment.append(pd.DataFrame(stix).tail(1).values.squeeze())
        stix_maximo.append(max_id)

# Cria dataframe com os momentos
df_moments = pd.DataFrame({'first_moment':stix_first_moment,'max_moment':stix_maximo,'last_moment':stix_last_moment})
#
momento = 'max_moment'
sti1 = pd.DataFrame(df_moments[momento].values, columns=['time'])
#  eliminate neighboors time (lasts of 10 hours and firsts of the next 10 hours)
##############################
sti_shift=sti1.shift(1)
dif=abs(sti1-sti_shift)
sti1['dif']=dif
#  mask with 180 minutes delta:
m = (sti1['dif'] >= pd.Timedelta('0 days 00:180:00')) 
sti1=sti1[m]
sti1=sti1.drop(columns=['dif'])
#################################################################################
sti=sti1[(sti1['time'].dt.month>=2) & (sti1['time'].dt.month<=5)] ####    WET 
#
#
st=sti - timedelta(hours=5)
#
end=sti + timedelta(hours=5)
##################################################################################                  
#  
###################################################################################################
#####################################################################################
#
#                     Selected serie - add to df the one you selected
#
#####################################################################################
case_fail = set()
#
df = pd.DataFrame([],columns=['case','tstep','time','var_1','var_2','var_3','var_12'])

for i, a in enumerate(sti.values):       
#
        ind=list(range(-10,11,1))
#
        for ti, tindex in enumerate(dfg_wet['time'].loc[st.iloc[i].values[0]:end.iloc[i].values[0]]):
                       
#
        # A instrução TRY foi utilizada abaixo porque o range de tempo no dataframe smpc_d1_325 é menor e gera um erro quando o tempo não é encontrado
           
            try:
                dt = {'case':i, 'tstep':ind[ti], 'time':tindex,
                'var_1':dfg_wet['var_1'].loc[tindex],
                'var_2':dfg_wet['var_2'].loc[tindex],
                'var_3':dfg_wet['var_3'].loc[tindex],
                'var_12':dfg_wet['var_12'].loc[tindex]}
                dfi=pd.DataFrame(dt,index=[(i)])
                df=pd.concat([df,dfi])
                
        # A instrução EXCEPT apenas guarda os casos em que ocorreram problemas
            except:
                case_fail.add(i)
##################################################################################################
df['tstep']=df['tstep']*0.5
#########################################################################################################################
#
#                  Write the data
#
#########################################################################################################################
print(df.head(60))
print(df.tail(60))
saidanc = df.to_xarray()
#
#       NEED TO SELECT THE OUTPUT AS FUNCTION OF THE COICE - MONOTERPENES OR ISOPRENE
#
#saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_tof_monoterpenes_'+str('var_12')+'.nc')
saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_tof_isoprene_'+str('var_12')+'.nc')
print('done wet')
