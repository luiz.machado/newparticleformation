import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import seaborn as sns
import warnings 
warnings.filterwarnings("ignore")
# opening database
print('var_1=CH4,var_2=CO2, var_3=CO,var_4=O3,var_5=NO,var_6=NO2,var_7=UFP,var_12=rainfall,var_13=radiation,var_14=isoprene,var_15=methanol,var_16=benzene,var_17=toluene,var_18=monoterpene')
var_name=sys.argv[1]
#
#################################################################################
tof = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/tof_profile.nc')
df_tof1=tof.to_dataframe()
df_tof1.index=pd.DatetimeIndex(df_tof1.index)
df_tof1['data']=df_tof1.index
df_tof1.index.names=['time']
df_tof=df_tof1.resample('30Min').mean()
#
####################################### OPEN AND SEASON CLASSIFY #################################################################
df2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/picarro_filtered.nc')
dfc=df2.to_dataframe()
dfc['CO2_79'].plot()
#
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/reactive_new.nc')
dfc1=df1.to_dataframe()

#######################################################
#            NUCL. Particles
####################################################
dt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine1.nc')
d1= pd.DataFrame()
d1=dt1.to_dataframe()
dt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine2.nc')
d2= pd.DataFrame()
d2=dt2.to_dataframe()
dt3= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine3.nc')
d3= pd.DataFrame()
d3=dt3.to_dataframe()
dt4= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine4.nc')
d4= pd.DataFrame()
d4=dt4.to_dataframe()
dt5= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine5.nc')
d5= pd.DataFrame()
d5=dt5.to_dataframe()
d1.index.names=['time']
d2.index.names=['time']
d3.index.names=['time']
d4.index.names=['time']
d5.index.names=['time']
########################################################
####################################################################################################################
# ABRE Tir
#
###################################################################################################################33
############################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/teta.nc')
tir = ds1.to_dataframe()[['Precipitation']]
#tir=tir[tir['Precipitation']>0.0]
df_tir=pd.DataFrame()
df_tir=tir.resample('30Min').max()
df_tir.index.names=['time']
################################## OPEN RADIATION #####################################################
ds2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/meteodata.nc')
tr = ds2.to_dataframe()
tr=tr[tr['Solar_inc_Wm2']<=1600]
tr=tr.resample('30Min').mean()
tr.index.names=['time']
########################################################
#              Build the dfg Array                     #
########################################################
date_rng = pd.date_range(start='2012-01-01 00:00:00',end='2020-10-31 23:30:00',freq='30T')
dfg =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18','time'])
#######################################################################################################
#   load alll valeus on the dfg array
#####################################################################
dfg['var_1']=dfc1['O3_0.05']
dfg['var_2']=dfc1['O3_0.5']
dfg['var_3']=dfc1['O3_4']
dfg['var_4']=dfc1['O3_12']
dfg['var_5']=dfc1['O3_24']
dfg['var_6']=dfc1['O3_38']
dfg['var_7']=dfc1['O3_53']
dfg['var_8']=dfc1['O3_79']
dfg['var_9']=d3['diam3']
dfg['var_10']=d4['diam4']
dfg['var_11']=d5['diam5']
dfg['var_12']=df_tir['Precipitation']
dfg['var_13']=tr['Solar_inc_Wm2']
dfg['var_14']=df_tof['isoprene_1']
dfg['var_15']=df_tof['methanol_1']
dfg['var_16']=df_tof['benzene_1']
dfg['var_17']=df_tof['toluene_1']
dfg['var_18']=df_tof['monoterpenes_1']
dfg['time']=dfg.index
################################################################
############################################################################
dfg['var_1']=dfg['var_1'].where(dfg['var_1']<=dfg['var_1'].quantile(0.999),np.nan)
dfg['var_2']=dfg['var_2'].where(dfg['var_2']<=dfg['var_2'].quantile(0.999),np.nan)
dfg['var_3']=dfg['var_3'].where(dfg['var_3']<=dfg['var_3'].quantile(0.999),np.nan)
dfg['var_4']=dfg['var_4'].where(dfg['var_4']<=dfg['var_4'].quantile(0.999),np.nan)
dfg['var_5']=dfg['var_5'].where(dfg['var_5']<=dfg['var_5'].quantile(0.999),np.nan)
dfg['var_6']=dfg['var_6'].where(dfg['var_6']<=dfg['var_6'].quantile(0.999),np.nan)
dfg['var_7']=dfg['var_7'].where(dfg['var_7']<=dfg['var_7'].quantile(0.999),np.nan)
dfg['var_8']=dfg['var_8'].where(dfg['var_8']<=dfg['var_8'].quantile(0.999),np.nan)
####################################################################
#           Select Range of Analysis
################################################################
dfg=dfg.loc['2018-01-01 00:00:00':'2020-10-30 00:00:00']
##################################################################
#        Define    filter outlier - eliminate  larger values the 0.0001 - keep 99.999% - xlim_out and select the larger values to be evaluated - quantile Xx
######################################################
# rain rate limit use thos code with 6,0 - 4.5 - 3.0 - 1.5 - 0.5
###############################
#xlim=6.0
#xlim=4.5
#xlim=3.0
#xlim=1.5
xlim=0.5
#
print('limit--> = ',xlim,var_name)
###########################################################################
############# split in dry and wet
####################################################################################
dfg_wet=dfg
###########################################################################
#
#                          filtered data

###############################found the First value , larger than xlim in the 10 hours slice  #################################################
###########################################################################

###### GET ALL MOMENTS ############################################################################
stix = []
stix_first_moment = []  # list para armazenamento dos primeiros momentos
stix_last_moment = []   # list para armazenamento dos ultimos momentos  
stix_maximo = []        # list para armazenamento dos maximos valores
groups_id=dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).groups.keys()

for gid in list(groups_id):

    stix = [] # list para armazenamento temporario de indices
    stiv = [] # list para armazenamento temporario de valores

    dfg_group = dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).get_group(gid)

    max_value = 0
    max_id = 0
    for row_id, row_value in dfg_group[var_name].iteritems():
        # print(gid,row_id,xlim,row_value)
        if(str(row_id)!=str(pd.NaT)):
            if(row_value >= xlim):
                stix.append(row_id)
                if (row_value != np.nan) & (row_value > max_value):
                    max_value = row_value
                    max_id = row_id

    # Em caso de NaT não armazena no dataframe
    if len(stix) != 0:
        stix_first_moment.append(pd.DataFrame(stix).head(1).values.squeeze())
        stix_last_moment.append(pd.DataFrame(stix).tail(1).values.squeeze())
        stix_maximo.append(max_id)
#
stix_last_moment=np.array(stix_last_moment)
stix_last_moment.astype(datetime)
#
# Cria dataframe com os momentos
df_moments = pd.DataFrame({'first_moment':stix_first_moment,'max_moment':stix_maximo,'last_moment':stix_last_moment})

############################################################################################
#  ESCOLHA O MOMENTO A SER CONSIDERADO
# momento = 'first_moment'
# momento = 'max_moment'
# momento = 'first_moment'
# momento = 'last_moment'
#################################################################################################
#
momento = 'max_moment'
sti1 = pd.DataFrame(df_moments[momento].values, columns=['time'])
#
###########################33
#  eliminate neighboors time (lasts of 10 hours and firsts of the next 10 hours)
##############################
sti_shift=sti1.shift(1)
dif=abs(sti1-sti_shift)
sti1['dif']=dif
#  mask with 180 minutes delta:
m = (sti1['dif'] >= pd.Timedelta('0 days 00:180:00')) 
sti1=sti1[m]
sti1=sti1.drop(columns=['dif'])
#################################################################################
#############           split in dry and wet and day and night           #############################
###############################           WET                         #####################################
sti=sti1
#
###########################################################################
############ filter for wet
###########################################################################
sti=sti[(sti['time'].dt.month>=2) & (sti['time'].dt.month<=5)] ####    WET 
#################################################################################
#
st=sti - timedelta(hours=5)
#
end=sti + timedelta(hours=5)
##################################################################################                  
#  
###################################################################################################
#####################################################################################
#
#                     Selected serie - add to df the one you selected
#
#####################################################################################
case_fail = set()
#
df = pd.DataFrame([],columns=['case','tstep','time','var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18'])

for i, a in enumerate(sti.values):       
#
        ind=list(range(-10,11,1))
#
        for ti, tindex in enumerate(dfg_wet['time'].loc[st.iloc[i].values[0]:end.iloc[i].values[0]]):
                       
#
        # A instrução TRY foi utilizada abaixo porque o range de tempo no dataframe smpc_d1_325 é menor e gera um erro quando o tempo não é encontrado
           
            try:
                dt = {'case':i, 'tstep':ind[ti], 'time':tindex,
                'var_1':dfg_wet['var_1'].loc[tindex],
                'var_2':dfg_wet['var_2'].loc[tindex],
                'var_3':dfg_wet['var_3'].loc[tindex],
                'var_4':dfg_wet['var_4'].loc[tindex],
                'var_5':dfg_wet['var_5'].loc[tindex],
                'var_6':dfg_wet['var_6'].loc[tindex],
                'var_7':dfg_wet['var_7'].loc[tindex],
                'var_8':dfg_wet['var_8'].loc[tindex],
                'var_9':dfg_wet['var_9'].loc[tindex],
                'var_10':dfg_wet['var_10'].loc[tindex],
                'var_11':dfg_wet['var_11'].loc[tindex],
                'var_12':dfg_wet['var_12'].loc[tindex],
                'var_13':dfg_wet['var_13'].loc[tindex],
                'var_14':dfg_wet['var_14'].loc[tindex],
                'var_15':dfg_wet['var_15'].loc[tindex],
                'var_16':dfg_wet['var_16'].loc[tindex],
                'var_17':dfg_wet['var_17'].loc[tindex],
                'var_18':dfg_wet['var_18'].loc[tindex]}
                dfi=pd.DataFrame(dt,index=[(i)])
                df=pd.concat([df,dfi])
                
        # A instrução EXCEPT apenas guarda os casos em que ocorreram problemas
            except:
                case_fail.add(i)
##################################################################################################
df['tstep']=df['tstep']*0.5
#########################################################################################################################
#
#                  Write the data
#
#########################################################################################################################
saidanc = df.to_xarray()
#saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_6.0_wet_'+str(var_name)+'xlim=4.5.nc')
#saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_4.5_wet_'+str(var_name)+'.nc')
#saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_3.0_wet_'+str(var_name)+'.nc')
#saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_1.5_wet_'+str(var_name)+'.nc')
saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_0.5_wet_'+str(var_name)+'.nc')
print('done wet')
#
