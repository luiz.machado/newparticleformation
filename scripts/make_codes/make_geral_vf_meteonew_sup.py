import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import seaborn as sns
import warnings 
warnings.filterwarnings("ignore")
# opening database
print('var_1=CH4,var_2=CO2, var_3=CO,var_4=O3,var_5=NO,var_6=NO2,var_7=UFP,var_12=rainfall,var_13=radiation,var_14=isoprene,var_15=methanol,var_16=benzene,var_17=toluene,var_18=MACR_MVK_ISOPOOH')
var_name=sys.argv[1]
#
#################################################################################
df = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/tof_profile.nc')
#
voc=df.to_dataframe()
#
voc['monoterpenes_1']=voc['monoterpenes_1'].where(voc['monoterpenes_1']<=voc['monoterpenes_1'].quantile(0.999),np.nan)
voc['monoterpenes_2']=voc['monoterpenes_2'].where(voc['monoterpenes_2']<=voc['monoterpenes_2'].quantile(0.999),np.nan)
voc['monoterpenes_3']=voc['monoterpenes_3'].where(voc['monoterpenes_3']<=voc['monoterpenes_3'].quantile(0.999),np.nan)
#
voc['isoprene_1']=voc['isoprene_1'].where(voc['isoprene_1']<=voc['isoprene_1'].quantile(0.999),np.nan)
voc['isoprene_2']=voc['isoprene_2'].where(voc['isoprene_2']<=voc['isoprene_2'].quantile(0.999),np.nan)
voc['isoprene_3']=voc['isoprene_3'].where(voc['isoprene_3']<=voc['isoprene_3'].quantile(0.999),np.nan)
#
voc['MACR_1']=voc['MACR_1'].where(voc['MACR_1']<=voc['MACR_1'].quantile(0.999),np.nan)
voc['MACR_2']=voc['MACR_2'].where(voc['MACR_2']<=voc['MACR_2'].quantile(0.999),np.nan)
voc['MACR_3']=voc['MACR_3'].where(voc['MACR_3']<=voc['MACR_3'].quantile(0.999),np.nan)
#
voc=voc.resample('30Min').mean()
#
voc.index.names=['time']
####################################### OPEN AND SEASON CLASSIFY #################################################################
df2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/picarro_filtered.nc')
dfc=df2.to_dataframe()
#
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/lycor_filtered.nc')
dfc1=df1.to_dataframe()
#######################################################
#            NUCL. Particles 60
####################################################
dt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine1.nc')
d1= pd.DataFrame()
d1=dt1.to_dataframe()
dt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine2.nc')
d2= pd.DataFrame()
d2=dt2.to_dataframe()
#
d1.index.names=['time']
d2.index.names=['time']
#
########################################################
#######################################################
#            NUCL. Particles 325
####################################################
dxt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine1.nc')
dx1= pd.DataFrame()
dx1=dxt1.to_dataframe()
dxt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine2.nc')
dx2= pd.DataFrame()
dx2=dxt2.to_dataframe()
#
dx1.index.names=['time']
dx2.index.names=['time']
#
####################################################################################################################
# ABRE rainfall
#
###################################################################################################################33
############################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/meteo_new.nc')
tir = ds1.to_dataframe()#[['Precipitation']]
df_tir=pd.DataFrame()
df_tir=tir.resample('30Min').mean()
df_tir.index.names=['time']
for i in df_tir.columns:
        print(i)
################################## OPEN RADIATION #####################################################
#######################################
#              Build the dfg Array                     #
########################################################
date_rng = pd.date_range(start='2012-01-01 00:00:00',end='2020-10-31 23:30:00',freq='30T')
dfg =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18','time'])
#######################################################################################################
#   load alll valeus on the dfg array
#####################################################################
dfg['var_1']=df_tir['T_26m']
dfg['var_2']=df_tir['RH_26m']
dfg['var_3']=df_tir['WSp_42m']
dfg['var_4']=df_tir['SW_in']
dfg['var_5']=dfc1['NO_0.5']
dfg['var_6']=dfc1['NO2_12']
dfg['var_7']=d1['diam1']
dfg['var_8']=d2['diam2']
dfg['var_9']=dx1['diam1']
dfg['var_10']=dx2['diam2']
dfg['var_11']=d1['diam1']
dfg['var_12']=df_tir['Rainfall']
dfg['var_13']=df_tir['PAR_in']
dfg['var_14']=voc['isoprene_1']
dfg['var_15']=voc['monoterpenes_1']
dfg['var_16']=voc['MACR_1']
dfg['var_17']=voc['toluene_1']
dfg['var_18']=voc['benzene_1']
dfg['time']=dfg.index
################################################################
#           Select Range of Analysis
################################################################
dfg=dfg.loc['2018-01-01 00:00:00':'2020-10-30 00:00:00']
##################################################################
#        Define    filter outlier - eliminate  larger values the 0.0001 - keep 99.999% - xlim_out and select the larger values to be evaluated - quantile Xx
######################################################
# Rain Rate limit
###############################
xlim=0.5
print('limit--> = ',xlim,var_name)
###########################################################################
############# split in dry and wet
####################################################################################
dfg_wet=dfg
###########################################################################
#
#                          filtered data

###############################found the First value , larger than xlim in the 10 hours slice  #################################################
###########################################################################

###### GET ALL MOMENTS ############################################################################
stix = []
stix_first_moment = []  # list para armazenamento dos primeiros momentos
stix_last_moment = []   # list para armazenamento dos ultimos momentos  
stix_maximo = []        # list para armazenamento dos maximos valores
groups_id=dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).groups.keys()

for gid in list(groups_id):

    stix = [] # list para armazenamento temporario de indices
    stiv = [] # list para armazenamento temporario de valores

    dfg_group = dfg_wet.groupby(pd.Grouper(key='time', freq="10H")).get_group(gid)

    max_value = 0
    max_id = 0
    for row_id, row_value in dfg_group[var_name].iteritems():
        # print(gid,row_id,xlim,row_value)
        if(str(row_id)!=str(pd.NaT)):
            if(row_value >= xlim):
                stix.append(row_id)
                if (row_value != np.nan) & (row_value > max_value):
                    max_value = row_value
                    max_id = row_id

    # Em caso de NaT não armazena no dataframe
    if len(stix) != 0:
        stix_first_moment.append(pd.DataFrame(stix).head(1).values.squeeze())
        stix_last_moment.append(pd.DataFrame(stix).tail(1).values.squeeze())
        stix_maximo.append(max_id)

# Cria dataframe com os momentos
df_moments = pd.DataFrame({'first_moment':stix_first_moment,'max_moment':stix_maximo,'last_moment':stix_last_moment})


#############################################################################################
#  ESCOLHA O MOMENTO A SER CONSIDERADO
# momento = 'first_moment'
# momento = 'max_moment'
# momento = 'first_moment'
# momento = 'last_moment'
#################################################################################################
#
momento = 'max_moment'
sti1 = pd.DataFrame(df_moments[momento].values, columns=['time'])
#
###########################
#  eliminate neighboors time (lasts of 10 hours and firsts of the next 10 hours)
##############################
sti_shift=sti1.shift(1)
dif=abs(sti1-sti_shift)
sti1['dif']=dif
#  mask with 180 minutes delta:
m = (sti1['dif'] >= pd.Timedelta('0 days 00:180:00')) 
sti1=sti1[m]
sti1=sti1.drop(columns=['dif'])
#################################################################################
#
#
#############           split in dry and wet and day and night           #############################
###############################           WET                         #####################################
sti=sti1
#
###########################################################################
############ filter for wet
###########################################################################
sti=sti[(sti['time'].dt.month>=2) & (sti['time'].dt.month<=5)] ####    WET 
#################################################################################
#
st=sti - timedelta(hours=5)
#
end=sti + timedelta(hours=5)
##################################################################################                  
#  
###################################################################################################
#####################################################################################
#
#                     Selected serie - add to df the one you selected
#
#####################################################################################
case_fail = set()
#
df = pd.DataFrame([],columns=['case','tstep','time','var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','var_13','var_14','var_15','var_16','var_17','var_18'])

for i, a in enumerate(sti.values):       
#
        ind=list(range(-10,11,1))
#
        for ti, tindex in enumerate(dfg_wet['time'].loc[st.iloc[i].values[0]:end.iloc[i].values[0]]):
                       
#
        # A instrução TRY foi utilizada abaixo porque o range de tempo no dataframe smpc_d1_325 é menor e gera um erro quando o tempo não é encontrado
           
            try:
                dt = {'case':i, 'tstep':ind[ti], 'time':tindex,
                'var_1':dfg_wet['var_1'].loc[tindex],
                'var_2':dfg_wet['var_2'].loc[tindex],
                'var_3':dfg_wet['var_3'].loc[tindex],
                'var_4':dfg_wet['var_4'].loc[tindex],
                'var_5':dfg_wet['var_5'].loc[tindex],
                'var_6':dfg_wet['var_6'].loc[tindex],
                'var_7':dfg_wet['var_7'].loc[tindex],
                'var_8':dfg_wet['var_8'].loc[tindex],
                'var_9':dfg_wet['var_9'].loc[tindex],
                'var_10':dfg_wet['var_10'].loc[tindex],
                'var_11':dfg_wet['var_11'].loc[tindex],
                'var_12':dfg_wet['var_12'].loc[tindex],
                'var_13':dfg_wet['var_13'].loc[tindex],
                'var_14':dfg_wet['var_14'].loc[tindex],
                'var_15':dfg_wet['var_15'].loc[tindex],
                'var_16':dfg_wet['var_16'].loc[tindex],
                'var_17':dfg_wet['var_17'].loc[tindex],
                'var_18':dfg_wet['var_18'].loc[tindex]}
                dfi=pd.DataFrame(dt,index=[(i)])
                df=pd.concat([df,dfi])
                
        # A instrução EXCEPT apenas guarda os casos em que ocorreram problemas
            except:
                case_fail.add(i)
##################################################################################################
df['tstep']=df['tstep']*0.5
#########################################################################################################################
#
#                  Write the data
#
#########################################################################################################################
saidanc = df.to_xarray()
saidanc.to_netcdf('/home/machado/work/gases/Gitlab/data_composite/dmax_meteo_wet_'+str(var_name)+'.nc')
print('done wet')
#
