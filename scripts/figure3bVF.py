import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator
import matplotlib as mpl
# #############################################################
# #
# #                                WET
# ##############################################################
color1='yellow'
color2='blue'
color3='red'
color4='green'
color5='black'
#################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_0.5_wet_var_12.nc')
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
#############################################################################################
#################################################################
df2 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_1.5_wet_var_12.nc')
df2_glm_q25= pd.DataFrame()
df2_glm_q25=df2.to_dataframe()
#############################################################################################
#################################################################
df3 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_3.0_wet_var_12.nc')
df3_glm_q25= pd.DataFrame()
df3_glm_q25=df3.to_dataframe()
#############################################################################################
#################################################################
df4 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_4.5_wet_var_12.nc')
df4_glm_q25= pd.DataFrame()
df4_glm_q25=df4.to_dataframe()
#############################################################################################
#################################################################
df5 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_O3_6.0_wet_var_12.nc')
df5_glm_q25= pd.DataFrame()
df5_glm_q25=df5.to_dataframe()
######################################################################################################
print(df_glm_q25)
print(df2_glm_q25)
print(df3_glm_q25)
print(df4_glm_q25)
print(df5_glm_q25)
#
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix
#
stix=[]
for i,ia in enumerate(df2_glm_q25['case']):
    stix.append(i)
df2_glm_q25.index=stix
#
stix=[]
for i,ia in enumerate(df3_glm_q25['case']):
    stix.append(i)
df3_glm_q25.index=stix
##
stix=[]
for i,ia in enumerate(df4_glm_q25['case']):
    stix.append(i)
df4_glm_q25.index=stix
#
stix=[]
for i,ia in enumerate(df5_glm_q25['case']):
    stix.append(i)
df5_glm_q25.index=stix
#############################################################################################################3
#
fig,ax=plt.subplots(figsize=(9,6))

filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig3b.jpg'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
plot1=sns.lineplot(x='tstep',y='var_1',data=df_glm_q25,ci=95,ax=ax,color=color1,lw=2, marker=('o'),label='Rain>0.5 mm.hr$^{-1}$')
plot1=sns.lineplot(x='tstep',y='var_1',data=df2_glm_q25,ci=95,ax=ax,color=color2,lw=2, marker=('x'),label='Rain>1.5 mm.hr$^{-1}$')
plot1=sns.lineplot(x='tstep',y='var_1',data=df3_glm_q25,ci=95,ax=ax,color=color3,lw=2, marker=('*'),label='Rain>3.0 mm.hr$^{-1}$')
plot1=sns.lineplot(x='tstep',y='var_1',data=df4_glm_q25,ci=95,ax=ax,color=color4,lw=2, marker=('x'),label='Rain>4.5 mm.hr$^{-1}$')
plot1=sns.lineplot(x='tstep',y='var_1',data=df5_glm_q25,ci=95,ax=ax,color=color5,lw=2, marker=('o'),label='Rain>6.0 mm.hr$^{-1}$')
#
plt.legend(ncol=1,loc=2,fontsize=15)
ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
ax.tick_params(axis='both', which='major', labelsize=15)
ax.spines['right'].set_linewidth(2)
ax.spines['bottom'].set_linewidth(2)
ax.spines['top'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)
#
#
sbox_to_anchor=(1, 0.5)
#
#plt.style.use('/home/machado/work/geral_files/presentation.mplstyle')
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=13)
ax.set_ylabel("  O3 Concentration (ppb)",fontsize=13)
#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
# ###################
