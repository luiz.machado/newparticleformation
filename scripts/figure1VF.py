
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 11:15:03 2023

@author: Gabriela.Unfer
"""
import xarray as xr
from matplotlib import colors
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from datetime import timedelta
from matplotlib.ticker import LogLocator, FuncFormatter, ScalarFormatter
import matplotlib.ticker

##################################################################################
################## LIST OF DATES #################################################
file = r'/home/machado/work/gases/Gitlab/dados_fonte/rainfall_events.txt'
dco=pd.read_csv(file,sep='\t')
df1=pd.DataFrame(dco)
df1['time']=pd.to_datetime(df1['time'], format='%Y/%m/%dT%H:%M:%S')
df1['time']=df1['time'].astype('datetime64[m]')
df1.index=df1['time']

##################################################################################
############## SMPS DATA #########################################################
ds1 = xr.open_dataset(r'/home/machado/work/gases/Gitlab/dados_fonte/ATTO-SMPS-clean_stp-transm-2014-202009_60m_correction_2021.nc')
ds11 = ds1.to_dataframe()
df_t = ds11.drop(['doy', 'dec.t'], axis=1)

#   filter larger values - elimintae the largest 0.0001 values
limiar=df_t.quantile(q=[0.98])
df_t=df_t.where(df_t<=limiar.values,np.nan)
 
df_temp=df_t.resample('5Min').mean()                                           
df_temp.index = df_temp.index.strftime('%Y-%m-%d %H:%M:%S')  
df_temp.index = pd.to_datetime(df_temp.index)

df2 = df_temp.copy()

###############################################################################################################
###############################################################################################################
################### PNSD from -5 to +5 hours by every 5min ###################################################

dataframe = pd.DataFrame()

for i in range (0,len(df1)):
          
    data = df2.loc[pd.to_datetime(df1['time'].iloc[i] - timedelta(minutes=300)) : pd.to_datetime(df1['time'].iloc[i] + timedelta(minutes=300))]
    data = data.reset_index()
    dataframe = pd.concat([dataframe,data])

dataframe = dataframe.drop(['Date'], axis=1)

########## DATAFRAME with all LAGS ############################################################################

df_all = dataframe.groupby(level=0).mean()  
df_all.index=np.arange(-300,305,5)

################################################################################
####################### FIGURE 1a ##############################################

diam = [10.2, 10.6, 10.9, 11.3, 11.8, 12.2, 12.6, 13.1, 13.6, 14.1, 14.6, 15.1, 15.7, 16.3, 16.8, 17.5, 18.1, 18.8, 19.5, 20.2, 
         20.9, 21.7, 22.5, 23.3, 24.1, 25.0, 25.9, 26.9, 27.9, 28.9, 30.0, 31.1, 32.2, 33.4, 34.6, 35.9, 37.2, 38.5, 40.0, 
         41.4, 42.9, 44.5, 46.1, 47.8, 49.6, 51.4, 53.3, 55.2, 57.3, 59.4, 61.5, 63.8, 66.1, 68.5, 71.0, 73.7, 76.4, 79.1, 
         82.0, 85.1, 88.2, 91.4, 94.7, 98.2, 101.8, 105.5, 109.4, 113.4, 117.6, 121.9, 126.3, 131.0, 135.8, 140.7, 145.9, 
         151.2, 156.8, 162.5, 168.5, 174.7, 181.1, 187.7, 194.6, 201.7, 209.1, 216.7, 224.7, 232.9, 241.4, 250.3, 259.5, 
         269.0, 278.8, 289.0, 299.6, 310.6, 322.0, 333.8, 346.0, 358.7, 371.8, 385.4, 399.5, 414.2]

###### CMAP
def inter_from_256(x):
    return np.interp(x=x,xp=[0,255],fp=[0,1])

R = [0,0,100,0,0,46,74,107,144,193,229,249,255,253,246]
G = [0,20,50,222,149,107,57,36,28,24,37,80,140,205,244]
B = [0,150,120,246,218,161,89,52,39,29,26,25,27,21,63]

R = [0,0,5,12,28,46,74,107,144,193,229,249,255,255,255]
G = [0,100,155,154,149,107,57,36,28,24,37,80,140,230,255]
B = [50,220,227,226,218,161,89,52,39,29,26,25,27,120,255]

cdict = {
    'red':((0.0,inter_from_256(R[0]),inter_from_256(R[0])),
           (1/14*1,inter_from_256(R[1]),inter_from_256(R[1])),
           (1/14*2,inter_from_256(R[2]),inter_from_256(R[2])),
           (1/14*3,inter_from_256(R[3]),inter_from_256(R[3])),
           (1/14*4,inter_from_256(R[4]),inter_from_256(R[4])),
           (1/14*5,inter_from_256(R[5]),inter_from_256(R[5])),
           (1/14*6,inter_from_256(R[6]),inter_from_256(R[6])),
           (1/14*7,inter_from_256(R[7]),inter_from_256(R[7])),
           (1/14*8,inter_from_256(R[8]),inter_from_256(R[8])),
           (1/14*9,inter_from_256(R[9]),inter_from_256(R[9])),
           (1/14*10,inter_from_256(R[10]),inter_from_256(R[10])),
           (1/14*11,inter_from_256(R[11]),inter_from_256(R[11])),
           (1/14*12,inter_from_256(R[12]),inter_from_256(R[12])),
           (1/14*13,inter_from_256(R[13]),inter_from_256(R[13])),
           (1.0,inter_from_256(R[14]),inter_from_256(R[14]))),
    'green': ((0.0,inter_from_256(G[0]),inter_from_256(G[0])),
           (1/14*1,inter_from_256(G[1]),inter_from_256(G[1])),
           (1/14*2,inter_from_256(G[2]),inter_from_256(G[2])),
           (1/14*3,inter_from_256(G[3]),inter_from_256(G[3])),
           (1/14*4,inter_from_256(G[4]),inter_from_256(G[4])),
           (1/14*5,inter_from_256(G[5]),inter_from_256(G[5])),
           (1/14*6,inter_from_256(G[6]),inter_from_256(G[6])),
           (1/14*7,inter_from_256(G[7]),inter_from_256(G[7])),
           (1/14*8,inter_from_256(G[8]),inter_from_256(G[8])),
           (1/14*9,inter_from_256(G[9]),inter_from_256(G[9])),
           (1/14*10,inter_from_256(G[10]),inter_from_256(G[10])),
           (1/14*11,inter_from_256(G[11]),inter_from_256(G[11])),
           (1/14*12,inter_from_256(G[12]),inter_from_256(G[12])),
           (1/14*13,inter_from_256(G[13]),inter_from_256(G[13])),
           (1.0,inter_from_256(G[14]),inter_from_256(G[14]))),
    'blue': ((0.0,inter_from_256(B[0]),inter_from_256(B[0])),
           (1/14*1,inter_from_256(B[1]),inter_from_256(B[1])),
           (1/14*2,inter_from_256(B[2]),inter_from_256(B[2])),
           (1/14*3,inter_from_256(B[3]),inter_from_256(B[3])),
           (1/14*4,inter_from_256(B[4]),inter_from_256(B[4])),
           (1/14*5,inter_from_256(B[5]),inter_from_256(B[5])),
           (1/14*6,inter_from_256(B[6]),inter_from_256(B[6])),
           (1/14*7,inter_from_256(B[7]),inter_from_256(B[7])),
           (1/14*8,inter_from_256(B[8]),inter_from_256(B[8])),
           (1/14*9,inter_from_256(B[9]),inter_from_256(B[9])),
           (1/14*10,inter_from_256(B[10]),inter_from_256(B[10])),
           (1/14*11,inter_from_256(B[11]),inter_from_256(B[11])),
           (1/14*12,inter_from_256(B[12]),inter_from_256(B[12])),
           (1/14*13,inter_from_256(B[13]),inter_from_256(B[13])),
           (1.0,inter_from_256(B[14]),inter_from_256(B[14]))),
}

cmap = colors.LinearSegmentedColormap('new_cmap',segmentdata=cdict)


##### PLOT
fig, ax = plt.subplots(figsize=(12,6),dpi=300)

x = np.arange(0,len(df_all),1)
y = diam
z = df_all.T
               
plt.pcolormesh(x,y,z,cmap=cmap,zorder=0)
ax.set_yscale('log')
   
def format_minor_ticks(x, pos):
      if pos % 2 == 0:
          return str(int(x))[0]
      else:
          return ""

ax.yaxis.set_minor_locator(LogLocator(base=10.0, subs=np.arange(1, 10)))
ax.yaxis.set_minor_formatter(FuncFormatter(format_minor_ticks))
ax.yaxis.set_tick_params(which='minor',length=3, labelsize=14)

ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=False))
ax.yaxis.set_tick_params(which='major', length=7,labelsize=16)
   
ax.grid(False)
ax.tick_params(axis='y', which='minor',direction='out', left=True)
ax.tick_params(axis='x', direction='out', bottom=True)
ax.tick_params(axis='y', direction='out', left=True)

labelx=np.arange(-5,+6,1)
labelx_with_sign = [f"+{x}" if x >= 1 else str(x) for x in labelx]
ax.set_xticks(np.arange(0,132,12))
ax.set_xticklabels(labelx_with_sign,fontsize=16,zorder=0)

cbar = plt.colorbar(orientation='vertical')
cbar.set_label('dN/dlogDp ($cm^{-3}$)',size=18)
cbar.ax.tick_params(labelsize=14)
ax.xaxis.set_minor_locator(plt.MultipleLocator(6))

plt.ylabel('Particle diameter ($nm$)',fontsize=18)
ax.set_xlabel("Hours before/after maximum rainfall",fontsize=18)

plt.savefig('/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig1a.jpg', bbox_inches='tight', pad_inches=0,dpi=300)
plt.show()

################################################################################
####################### FIGURE S.21b ##############################################

########## DATAFRAME with the median from -3h to -1h AND +1h to +3 ###############################################
#### idx 60 is 0; every idx is 5min

minus_3 = df_all.iloc[24:48].median(axis=0)
plus_3 = df_all.iloc[52:96].median(axis=0)

df_3 = pd.concat([minus_3,plus_3],axis=1)
df_3 = df_3.T

####

diam = [10.2, 10.6, 10.9, 11.3, 11.8, 12.2, 12.6, 13.1, 13.6, 14.1, 14.6, 15.1, 15.7, 16.3, 16.8, 17.5, 18.1, 18.8, 19.5, 20.2, 
         20.9, 21.7, 22.5, 23.3, 24.1, 25.0, 25.9, 26.9, 27.9, 28.9, 30.0, 31.1, 32.2, 33.4, 34.6, 35.9, 37.2, 38.5, 40.0, 
         41.4, 42.9, 44.5, 46.1, 47.8, 49.6, 51.4, 53.3, 55.2, 57.3, 59.4, 61.5, 63.8, 66.1, 68.5, 71.0, 73.7, 76.4, 79.1, 
         82.0, 85.1, 88.2, 91.4, 94.7, 98.2, 101.8, 105.5, 109.4, 113.4, 117.6, 121.9, 126.3, 131.0, 135.8, 140.7, 145.9, 
         151.2, 156.8, 162.5, 168.5, 174.7, 181.1, 187.7, 194.6, 201.7, 209.1, 216.7, 224.7, 232.9, 241.4, 250.3, 259.5, 
         269.0, 278.8, 289.0, 299.6, 310.6, 322.0, 333.8, 346.0, 358.7, 371.8, 385.4, 399.5, 414.2]

fig, ax = plt.subplots(figsize=(12,6),dpi=300)
plt.plot(diam,minus_3,label='Before',color='red',lw=3)
plt.plot(diam,plus_3,label='After',color='blue',lw=3)
plt.ylabel('dN/dlogDp ($cm^{-3}$)',fontsize=20)
ax.set_xlabel("Particle diameter ($nm}$)",fontsize=20)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.legend(fontsize=16)
plt.xscale('log')
plt.yscale('log')
plt.grid(which='both',alpha=0.5,ls='-')
plt.xlim([9.5,450])
plt.ylim(1,700)

x_major = matplotlib.ticker.LogLocator(base = 10.0, numticks = 2)
ax.xaxis.set_major_formatter(ScalarFormatter(useMathText=False))
ax.xaxis.set_major_locator(x_major)

def format_minor_ticks(x, pos):
      if pos % 2 == 0:
          return str(int(x))[0]
      else:
          return ""

ax.xaxis.set_minor_locator(LogLocator(base=10.0, subs=np.arange(1, 10)))
ax.xaxis.set_minor_formatter(FuncFormatter(format_minor_ticks))
ax.xaxis.set_tick_params(which='minor',length=3, labelsize=14)
plt.savefig('/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig1b.jpg', bbox_inches='tight', pad_inches=0,dpi=300)
plt.show()


