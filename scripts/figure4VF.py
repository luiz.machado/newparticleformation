import xarray as xr
import math
import pandas as pd
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator,MultipleLocator
#
#######################################################################################################
###########################################################################################################
#           WET
#
################################################################################################
#
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_wet_var_12.nc')
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig4.jpg'
#
#########################################################################################################
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
print(df_glm_q25)
#############################################################################################
#############################################################################################
print(df_glm_q25)
dff=df_glm_q25
dff.index=df_glm_q25['tstep']
ix=0
ixx=0
for i in range(len(dff)):
    if(dff.index[i]==-2): 
        ixx=ixx+1
for i in range(len(dff)):
    if(dff.index[i]==-2 and math.isnan(dff['var_1'].iloc[i])): 
        ix=ix+1
print('total values geral =  ',ixx)
print('total values on composite=  ',ixx-ix)
#
#############################################################################################
##
#                               Change average
##############################################################################################
df_glm_q25['var_1']=(df_glm_q25['var_1']-df_glm_q25['var_1'].mean())/df_glm_q25['var_1'].std()
df_glm_q25['var_2']=(df_glm_q25['var_2']-df_glm_q25['var_2'].mean())/df_glm_q25['var_2'].std()
df_glm_q25['var_3']=(df_glm_q25['var_3']-df_glm_q25['var_3'].mean())/df_glm_q25['var_3'].std()
df_glm_q25['var_4']=(df_glm_q25['var_4']-df_glm_q25['var_4'].mean())/df_glm_q25['var_4'].std()
df_glm_q25['var_5']=(df_glm_q25['var_5']-df_glm_q25['var_5'].mean())/df_glm_q25['var_5'].std()
df_glm_q25['var_6']=(df_glm_q25['var_6']-df_glm_q25['var_6'].mean())/df_glm_q25['var_6'].std()
df_glm_q25['var_8']=df_glm_q25['var_7']+df_glm_q25['var_8']
df_glm_q25['var_9']=(df_glm_q25['var_9']-df_glm_q25['var_9'].mean())/df_glm_q25['var_9'].std()
df_glm_q25['var_10']=(df_glm_q25['var_10']-df_glm_q25['var_10'].mean())/df_glm_q25['var_10'].std()
df_glm_q25['var_11']=(df_glm_q25['var_11']-df_glm_q25['var_11'].mean())/df_glm_q25['var_11'].std()
df_glm_q25['var_12']=(df_glm_q25['var_12']-df_glm_q25['var_12'].mean())/df_glm_q25['var_12'].std()
df_glm_q25['var_13']=(df_glm_q25['var_13']-df_glm_q25['var_13'].mean())/df_glm_q25['var_13'].std()
df_glm_q25['var_13']=(df_glm_q25['var_13']-df_glm_q25['var_13'].mean())/df_glm_q25['var_13'].std()
df_glm_q25['var_14']=(df_glm_q25['var_14']-df_glm_q25['var_14'].mean())/df_glm_q25['var_14'].std()
df_glm_q25['var_15']=(df_glm_q25['var_15']-df_glm_q25['var_15'].mean())/df_glm_q25['var_15'].std()
df_glm_q25['var_16']=(df_glm_q25['var_16']-df_glm_q25['var_16'].mean())/df_glm_q25['var_16'].std()
df_glm_q25['var_17']=(df_glm_q25['var_17']-df_glm_q25['var_17'].mean())/df_glm_q25['var_17'].std()
df_glm_q25['var_18']=(df_glm_q25['var_18']-df_glm_q25['var_18'].mean())/df_glm_q25['var_18'].std()
###########################################################################
############################################
fig,ax=plt.subplots(figsize=(10,7))
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_major_locator(MultipleLocator(1))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix
#
#
ax2=ax.twinx()
ax2.spines['right'].set_color('red')
#
sns.lineplot(x='tstep',y='var_4',data=df_glm_q25,ci=95,ax=ax,color='blue',lw=2,linestyle = 'solid',label='O3 H=4 m')
sns.lineplot(x='tstep',y='var_5',data=df_glm_q25,ci=95,ax=ax,color='yellow',lw=2,linestyle = 'dashdot',label='NO H=0.5 m')
sns.lineplot(x='tstep',y='var_6',data=df_glm_q25,ci=95,ax=ax,color='cyan',lw=2,linestyle = 'dotted',label='NO2 H=12 m')
sns.lineplot(x='tstep',y='var_8',data=df_glm_q25,ci=95,ax=ax2,color='red',lw=3,linestyle = 'dashed',marker='*',label='UFP10-40 H=60 m')
sns.lineplot(x='tstep',y='var_14',data=df_glm_q25,ci=95,ax=ax,color='green',lw=3,linestyle = 'solid',marker='*',label='Isoprene H=80 m')
sns.lineplot(x='tstep',y='var_15',data=df_glm_q25,ci=95,ax=ax,color='black',lw=3,linestyle = 'dashdot',marker='x',label='Monoterpene H=80 m')
ax.legend(loc=2,fontsize=12)
#
sbox_to_anchor=(1, 0.5)
#
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate ",fontsize=15)
ax.set_ylabel("Stand. Dev. Gases",fontsize=15)
ax2.set_ylabel("UFP 20-40 nm-  Num. Con.  [$cm^{-3}$]",color='red',fontsize=15)
ax2.tick_params(axis='y', colors='red')
ax.tick_params(axis='both', which='major', labelsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax.spines['left'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
#
plt.tight_layout()
plt.savefig(filename,format='jpg',dpi=300)
plt.show()
#