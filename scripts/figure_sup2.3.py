# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 12:12:17 2022

@author: Gabriela.Unfer
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import timedelta
import scipy.stats as st
from pandas.tseries.frequencies import to_offset

######################## SMPS modes ####################################################
path2 =r'/home/machado/work/gases/Gitlab/dados_fonte/All_B2_SMPS-clean-stp-transm-SMPS-60m-FEV22aABR22_resample_modos_upd2.csv'

dados2 = pd.read_csv(path2)
df2 = pd.DataFrame(dados2)
df2['Date_Hour'] = pd.to_datetime(df2['Date_Hour'], format='%Y%m%d_%H%M')
df2.index=df2['Date_Hour']
df2 = df2.drop(['Date_Hour'], axis=1)
#print(df2)

##################################### Wvel - LAP #######################################
path3=r'/home/machado/work/gases/Gitlab/dados_fonte/All_B2_FEV2022aABR2022_LAP_Wvel.csv'

dados3 = pd.read_csv(path3,sep=',')
df3 = pd.DataFrame(dados3)
df3['Date_Hour'] = pd.to_datetime(df3['Date_Hour'], format='%Y%m%d_%H%M')
df3.index=df3['Date_Hour']
df3 = df3.drop(['Date_Hour'], axis=1)
df3 = df3.astype(float)

##########################################################################################
################################ FIELD MILL #############################################
path4=r'/home/machado/work/gases/Gitlab/dados_fonte/All_B2_FieldMill_FEVaABR2022_1min.csv'


dados4 = pd.read_csv(path4,sep=',')
df4 = pd.DataFrame(dados4)
df4.index=df4['Unnamed: 0']
df4.index = df4.index.rename('Date_Hour')
df4 = df4.drop(['Unnamed: 0'], axis=1)
df4.index = pd.to_datetime(df4.index)
df4 = df4.astype(float)

df4 = df4.resample('5min').apply(lambda x: max(x,key=abs))
df4 = df4.reset_index()
df4 = df4.rename(columns={'index': 'Date_Hour'})
df4.index = df4['Date_Hour']
df4 = df4.drop(['Date_Hour'],axis=1)
df4.index = df4.index + to_offset("5min")
df4 = df4.abs()
df4[df4['Avg']<=200] = np.nan

#######################################################################################
######################## Ozone 79m ####################################################
path5 =r'/home/machado/work/gases/Gitlab/dados_fonte/All_B2_Gases_Valve8_79m_FEV2022aABR2022.csv'

dados5 = pd.read_csv(path5)
df5 = pd.DataFrame(dados5)
df5.index=df5['DateTime']
df5 = df5.drop(['DateTime'], axis=1)
df5.index=pd.DatetimeIndex(df5.index)
df5.index = df5.index + to_offset("30min")
df5 = df5.resample('5min').ffill()

#######################################################################################

################### Events with Wvel stronger or equal to -1 m/s ######################

#######################################################################################
df_Wvel = df3['W_quente'] 
df_Wvel[df_Wvel > -1] = np.nan
df_Wvel = df_Wvel.reset_index()
df_Wvel = df_Wvel[df_Wvel['W_quente'].notna()]

z = df_Wvel['Date_Hour'].diff().dt.seconds.gt(899).cumsum()  #eventos separados por mais de 15min 

df_start1 = pd.DataFrame()
df_end1 = pd.DataFrame()
df_start1 = df_Wvel.groupby(z, as_index=False).first()
df_end1 = df_Wvel.groupby(z, as_index=False).last()

df_events = pd.DataFrame(columns=['Start','End'])
df_events['Start'] = df_start1['Date_Hour']
df_events['End'] = df_end1['Date_Hour']


##################################################################

dUAP = []
dUAP2 = []
dUAP_total_list = []
UAP0_list=[]
UAP0_list2=[]
UAP0_list_total=[]
UAP0_hour=[]
maxUAP_list=[]
maxUAP_list2=[]
maxUAP_total_list=[]
maxUAP_hour = []
maxUAP_hour2 = []
maxUAP_hour_total_list = []
Wvel_list = []
Wvel_list2 = []
Elect_field = []
idx_E = []
o3_event_max =[]
o3_0 = []
mean_O3 = []
mean_O3_1 = []
mean_O3_2 = []
mean_O3_3 = []
mean_O3_4 = []
mean_O3_5 = []
mean_O3_6 = []
mean_O3_7 = []
lista_inicio_eventos = []


time = 90
for i in range (0,len(df_events)):
    
    start = df_events['Start'][i]
    end = df_events['End'][i]
    lista_inicio_eventos.append(start)
        
    O3 = (df5['O349i_Avg'].loc[start : end].max())

    var_max= (df3['W_quente'].loc[start : end].min()) 
    idx_max= (df3['W_quente'].loc[start : end].idxmin())
    
    E = (df4['Avg'].loc[start : idx_max].max())  
    E_idx = (df4['Avg'].loc[start : idx_max].idxmax()) 

    UAP_0 = (df2['Ultrafine'].loc[idx_max])/64  
    UAP0_hour.append((idx_max))
    UAPmax= (df2['Ultrafine'].loc[idx_max : pd.to_datetime(idx_max + timedelta(minutes=time))].max())/64  
    UAPmax_idx = df2['Ultrafine'].loc[idx_max: pd.to_datetime(idx_max + timedelta(minutes=time))].idxmax()
    
    O3_0 = (df5['O349i_Avg'].loc[idx_max])
    try:        
        O3_mean2 = (df5['O349i_Avg'].loc[E_idx])
        O3_mean3 = (df5['O349i_Avg'].loc[UAPmax_idx])
        O3_mean6 = (df5['O349i_Avg'].loc[idx_max])        ### Used to plot !!!
        O3_mean7 = (df5['O349i_Avg'].loc[start])
                
        mean_O3_2.append(O3_mean2)
        mean_O3_3.append(O3_mean3) 
        mean_O3_6.append(O3_mean6) 
        mean_O3_7.append(O3_mean7) 
        
    except (KeyError):
      
        mean_O3_2.append(np.nan)
        mean_O3_3.append(np.nan)
        mean_O3_6.append(np.nan)
        mean_O3_7.append(np.nan) 
    
    try:        
        O3_mean = (df5['O349i_Avg'].loc[start : idx_max].max())
        O3_mean1 = (df5['O349i_Avg'].loc[idx_max : UAPmax_idx].max())
        O3_mean4 = (df5['O349i_Avg'].loc[start : UAPmax_idx].max())
        O3_mean5 = (df5['O349i_Avg'].loc[start : idx_max].max())
        
        mean_O3.append(O3_mean)
        mean_O3_1.append(O3_mean1)
        mean_O3_4.append(O3_mean4)
        mean_O3_5.append(O3_mean5)
   
    except (TypeError) :
        mean_O3.append(np.nan)
        mean_O3_1.append(np.nan)
        mean_O3_4.append(np.nan)
        mean_O3_5.append(np.nan)

    
    value = (UAPmax-UAP_0)
    
    #Total
    UAP0_list_total.append(UAP_0)
    maxUAP_total_list.append(UAPmax)
    dUAP_total_list.append(value)
    maxUAP_hour_total_list.append(UAPmax_idx)      
    Wvel_list.append(var_max)
    Elect_field.append(E)
    o3_0.append(O3_0)
    o3_event_max.append(O3)
    idx_E.append(E_idx)
       
######################################################################################

#Dataframe total
df_dt = pd.DataFrame()
df_dt['dUAP_total'] = dUAP_total_list
df_dt['UAP_0_total'] = UAP0_list_total
df_dt['UAPmax_total'] = maxUAP_total_list

df_total = pd.DataFrame()
df_total['Initial_hour'] = UAP0_hour
df_total['maxUAP_hour_total'] = maxUAP_hour_total_list
df_total['UAP_0_total'] = df_dt['UAP_0_total']
df_total['UAPmax_total'] = df_dt['UAPmax_total']
df_total['dUAP_total'] = df_dt['dUAP_total']
df_total['dUAP_total_perc'] = ((df_dt['dUAP_total'])/df_dt['UAP_0_total'])*100
df_total['dt_total'] = (df_total['maxUAP_hour_total'] - df_total['Initial_hour']).dt.total_seconds()/60
df_total['Wvel'] = Wvel_list
df_total['Elect_field'] = Elect_field
df_total['O3_event_max'] = o3_event_max
df_total['O3_0'] = o3_0
df_total['O3_mean'] = mean_O3
df_total['O3_mean1'] = mean_O3_1
df_total['O3_mean2'] = mean_O3_2
df_total['O3_mean3'] = mean_O3_3
df_total['O3_mean4'] = mean_O3_4
df_total['O3_mean5'] = mean_O3_5
df_total['O3_mean6'] = mean_O3_6
df_total['O3_mean7'] = mean_O3_7
df_total['Inicio_Eventos'] = lista_inicio_eventos


for j in range (1, len(df_total)): #Filter maximums with same time; take the closest one
    if maxUAP_hour_total_list[j-1] == maxUAP_hour_total_list[j]:
        df_total.loc[j-1] = np.nan

df_total = df_total[df_total['UAP_0_total'].notna()]
df_total = df_total[df_total['UAPmax_total'].notna()]
df_total = df_total[df_total['dUAP_total'].notna()]

df_total['Hora_inicial'] = df_total['Inicio_Eventos'].dt.hour - 4


################### HISTOGRAMA TOTAL ###################################

b1 = len(df_total.query('0<=dt_total<15'))
b2 = len(df_total.query('15<=dt_total<30'))
b3 = len(df_total.query('30<=dt_total<45'))
b4 = len(df_total.query('45<=dt_total<60'))
b5 = len(df_total.query('60<=dt_total<75'))
b6 = len(df_total.query('75<=dt_total<=90'))

bins_total = pd.Series([b1,b2,b3,b4,b5,b6])

########################################################################

# BINS
var = 'dUAP_total'
b1 = df_total.query('dt_total<15')[var]
b2 = df_total.query('15<=dt_total<30')[var]
b3 = df_total.query('30<=dt_total<45')[var]
b4 = df_total.query('45<=dt_total<60')[var]
b5 = df_total.query('60<=dt_total<75')[var]
b6 = df_total.query('75<=dt_total<=90')[var]

series = pd.Series([b1,b2,b3,b4,b5,b6])

ci_min=[]
ci_max=[]
for t in series:
    t.dropna(axis=0)
    x = st.t.interval(0.95, len(t)-1, loc=t.mean(), scale=st.sem(t,nan_policy='omit'))
    ci_min.append(x[0])
    ci_max.append(x[1])

mean = pd.Series([b1.mean(),b2.mean(),b3.mean(),b4.mean(),b5.mean(),b6.mean()])

# BINS
var = 'O3_mean6'                               ## Ozone at maximum downdraft
b1 = df_total.query('dt_total<15')[var]
b2 = df_total.query('15<=dt_total<30')[var]
b3 = df_total.query('30<=dt_total<45')[var]
b4 = df_total.query('45<=dt_total<60')[var]
b5 = df_total.query('60<=dt_total<75')[var]
b6 = df_total.query('75<=dt_total<=90')[var]

series = pd.Series([b1,b2,b3,b4,b5,b6])

ci_min2=[]
ci_max2=[]
for t in series:
    t.dropna(axis=0)
    x = st.t.interval(0.95, len(t)-1, loc=t.mean(), scale=st.sem(t,nan_policy='omit'))
    ci_min2.append(x[0])
    ci_max2.append(x[1])

mean2 = pd.Series([b1.mean(),b2.mean(),b3.mean(),b4.mean(),b5.mean(),b6.mean()])

series_O3 = pd.DataFrame([b1,b2,b3,b4,b5,b6])


####################################################################################
############################### PLOT ###############################################
####################################################################################

fig,ax = plt.subplots(figsize=(12,6),dpi=200) 
ax2=ax.twinx()
ax3=ax.twinx()

ax3.spines.right.set_position(("axes", 1.15))

color='black'

x=np.arange(0,90,15)
ax.bar(x=x,height=(bins_total/bins_total.sum())*100,width=10,edgecolor=color,color=color,alpha=0.5)
plt.xticks([0,15, 30, 45,60,75], ['[0-15)', '[15-30)', '[30-45)','[45-60)','[60-75)','[75-90]'])

color2 = 'red'
a=np.arange(0,90,15)
p2,=ax2.plot(a,mean,ls='-',lw=4,color=color2,label=u'mean Δ(Ultrafine) conc')
ax2.fill_between(a,ci_max, ci_min,color=color2,alpha=0.3)

color3 = 'green'
a=np.arange(0,90,15)
p3,=ax3.plot(a,mean2,ls='--',lw=4,color=color3,label=u'mean O3 conc')
ax3.fill_between(a,ci_max2, ci_min2,color=color3,alpha=0.3)

size=18
ax2.set_ylabel("Sub-50 nm increment ($cm^{-3}$)",color=color2,fontsize=size)
ax3.set_ylabel("Ozone at maximum donwdraft ($ppb$)",color=color3,fontsize=size)
ax.set_ylabel("Frequency (%)",color=color,fontsize=size)
ax2.yaxis.label.set_color(color2)
ax3.yaxis.label.set_color(color3)
ax.set_xlabel(u'Δt (minutes)',fontsize=size)

ax.set_ylim([0,35])
ax3.set_ylim([4,11])

tkw = dict(size=10, width=1.5)
size=17
ax.tick_params(axis='y',colors=color, **tkw,labelsize=size)
ax2.tick_params(axis='y', colors=p2.get_color(), **tkw,labelsize=size)
ax3.tick_params(axis='y', colors=p3.get_color(), **tkw,labelsize=size)
ax.tick_params(axis='x',labelsize=size)

ax.xaxis.set_major_locator(plt.MultipleLocator(15))
ax.yaxis.set_major_locator(plt.MultipleLocator(5))
ax.yaxis.set_minor_locator(plt.MultipleLocator(2.5))

plt.tight_layout()
plt.savefig(r'/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.3.jpg',dpi=300,pad_inches=0.1,bbox_inches='tight')
plt.show()

