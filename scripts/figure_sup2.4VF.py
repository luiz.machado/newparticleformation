import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator
import matplotlib as mpl
# #############################################################
# #
# #                                
# ##############################################################
color1='black'
color2='gray'
color3='red'
color4='orange'
color5 = 'yellow'
color6='green'
color7='cyan'
color8='blue'
#############################################################################################
#
#################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_NO_wet_var_12.nc')
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
#############################################################################################
##
#                               NO
######################################################################################################
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix
#
##
fig,ax=plt.subplots(figsize=(10,8))
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.4.jpg'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
plot1=sns.lineplot(x='tstep',y='var_1',data=df_glm_q25,ci=95,ax=ax,color=color1,lw=2, marker=('o'),label='H=0.05 m')
plot1=sns.lineplot(x='tstep',y='var_2',data=df_glm_q25,ci=95,ax=ax,color=color2,lw=2, marker=('x'),label='H=0.5 m')
plot1=sns.lineplot(x='tstep',y='var_3',data=df_glm_q25,ci=95,ax=ax,color=color3,lw=2, marker=('*'),label='H=4 m')
plot1=sns.lineplot(x='tstep',y='var_4',data=df_glm_q25,ci=95,ax=ax,color=color4,lw=2, marker=('x'),label='H=12 m')
#
plt.legend(ncol=4,loc=2,fontsize=13)
ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
#
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=13)
ax.set_ylabel("  NO Concentration (ppb)",fontsize=13)
ax.tick_params(axis='both', which='major', labelsize=13)
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()