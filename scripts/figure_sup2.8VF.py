import xarray as xr
import math
import pandas as pd
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator,MultipleLocator
#################################################################
#######################################################################################################
############################################
fig,ax=plt.subplots(figsize=(14,7))
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_meteo_wet_var_12.nc')
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.8.jpg'
#plt.title('All Cases Composite')
#########################################################################################################
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
print(df_glm_q25)
#############################################################################################
#############################################################################################
print(df_glm_q25)
dff=df_glm_q25
dff.index=df_glm_q25['tstep']
#
###########################
##
#                               Change average
##############################################################################################
###########################################################################
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_major_locator(MultipleLocator(1))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
ax2=ax.twinx()
ax2.spines['right'].set_color('blue')

ax3=ax.twinx()
ax3.spines['right'].set_color('green')
ax3.spines['right'].set_position(('outward', 60)) 
ax4=ax.twinx()
ax4.spines['right'].set_color('red')
ax4.spines['right'].set_position(('outward', 110)) 
#
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix

#
#
sns.lineplot(x='tstep',y='var_1',data=df_glm_q25,ci=95,ax=ax,color='black',lw=2,linestyle = 'solid',label='Temperature 26 m',legend=False)
sns.lineplot(x='tstep',y='var_2',data=df_glm_q25,ci=95,ax=ax2,color='blue',lw=2,linestyle = 'dashdot',label='Rel. Humidity 26 m',legend=False)
sns.lineplot(x='tstep',y='var_3',data=df_glm_q25,ci=95,ax=ax3,color='green',lw=2,linestyle = 'dotted',label='Wind_Speed 42 m',legend=False)
sns.lineplot(x='tstep',y='var_4',data=df_glm_q25,ci=95,ax=ax4,color='red',lw=2,linestyle = 'solid',marker='o',label='Solar Inc. Rad.',legend=False)

h1, l1 = ax.get_legend_handles_labels()
h2, l2 = ax2.get_legend_handles_labels()
h3, l3 = ax3.get_legend_handles_labels()
h4, l4 = ax4.get_legend_handles_labels()
#
plt.legend(h1+h2+h3+h4,l1+l2+l3+l4, ncol=2,loc=1,fontsize=13)
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=13)
ax.set_ylabel("Temperature (C)",fontsize=13)
ax2.set_ylabel("Relative Humidity (%)",fontsize=13)
ax3.set_ylabel("Wind Speed ($m.s^⁻1$)",fontsize=13)
ax4.set_ylabel("Solar Radiation ($W.m^⁻2$)",fontsize=13)
#
ax.tick_params(axis='both', which='major', labelsize=13)
ax2.tick_params(axis='both', which='major', labelsize=13)
ax3.tick_params(axis='both', which='major', labelsize=13)
ax4.tick_params(axis='both', which='major', labelsize=13)
#
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
ax2.spines['right'].set_linewidth(3)
ax3.spines['right'].set_linewidth(3)
ax4.spines['right'].set_linewidth(3)
#
plt.tight_layout()
plt.savefig(filename,format='jpg',dpi=300)
plt.show()
