from matplotlib import markers
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import seaborn as sns
import warnings 
import pytz
#
# opening database
#############################################################
###################################################################################################################
acms = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/acms_bruno.nc')
#
df_acms1=acms.to_dataframe()
df_acms1.index=pd.DatetimeIndex(df_acms1.index)
df_acms1.index.names=['time']
df_acms=df_acms1.resample('30Min').mean()
print(df_acms)

#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
df_acms.index=df_acms.index.tz_localize('UTC')
df_acms.index=df_acms.index.tz_convert(local_timezone)
print(df_acms)
#
df_acms=df_acms.loc['2021-02-01 00:00:00':'2022-05-31 00:00:00']
df_acms=df_acms[(df_acms.index.month>=2) & (df_acms.index.month<=5)] ####    WET 
#
fig,ax=plt.subplots(figsize=(9,6.5))
#
#
sns.lineplot(x=df_acms.index.hour,y='org44-C4',data=df_acms,ci=95,ax=ax,color='r',label='H=60 m',lw=2.5)
plt.legend(ncol=1,loc=2)
ax.legend(loc=2,fontsize=13)
ax.set_ylabel('Org44 H=60m - Oxigenated Aerosol $ug.m^{-3}$',color='r',fontsize=13)
ax.set_xlabel('Local Time',fontsize=13)
ax2=ax.twinx()
sns.lineplot(x=df_acms.index.hour,y='org44-A1',data=df_acms,ci=95,ax=ax2,color='b',label='H=325 m',lw=2.5)
ax2.set_ylabel('Org44 H=325m - Oxigenated Aerosol $ug.m^{-3}$',color='b',fontsize=13)
plt.legend(ncol=1,loc=1,fontsize=13)
ax.set_xticks(range(24))
ax.spines['right'].set_color('blue')
ax2.spines['left'].set_color('red')
ax.tick_params(axis='both', which='major', labelsize=13)
ax2.tick_params(axis='both', which='major', labelsize=13)
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
plt.tight_layout()
#
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.12.jpg'
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
#
#
fig,ax=plt.subplots(figsize=(9,6))
#
#
df_acms['Org-C4']=df_acms['Org-C4'][df_acms['Org-C4']<=1.0]
df_acms['Org-A1']=df_acms['Org-A1'][df_acms['Org-A1']<=1.0]
#
sns.lineplot(x=df_acms.index.hour,y='Org-C4',data=df_acms,ci=95,ax=ax,color='r',label='H=60 m',lw=2.5)
plt.legend(ncol=1,loc=2, fontsize=13)
ax.set_ylabel('Org H=60m - Organic Aerosol $ug.m^{-3}$',color='r', fontsize=13)
ax.set_xlabel('Local Time', fontsize=13)
ax2=ax.twinx()
sns.lineplot(x=df_acms.index.hour,y='Org-A1',data=df_acms,ci=95,ax=ax2,color='b',label='H=325 m',lw=2.5)
ax2.set_ylabel('Org H=325m - Organic Aerosol $ug.m^{-3}$',color='b', fontsize=13)
plt.legend(ncol=1,loc=1, fontsize=13)
ax2.set_ylim(0.11, 0.28)
ax.set_xticks(range(24))
ax.spines['right'].set_color('blue')
ax2.spines['left'].set_color('red')
ax.tick_params(axis='both', which='major', labelsize=13)
ax2.tick_params(axis='both', which='major', labelsize=13)
ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
plt.tight_layout()
#
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.13.jpg'
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
#
