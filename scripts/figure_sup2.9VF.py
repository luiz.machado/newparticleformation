import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import warnings 
import math
from scipy.stats import gaussian_kde
import seaborn as sns
# opening database
#
#################################################################################
df = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/tof_profile.nc')
voc=df.to_dataframe()
voc=voc.resample('30Min').mean()
voc.index.names=['time']
####################################### OPEN AND SEASON CLASSIFY #################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/lycor_filtered.nc')
dfc=df1.to_dataframe()
dfc=dfc.resample('30Min').mean()
dfc.index.names=['time']
#######################################################
#            NUCL. Particles 60
####################################################
dt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine1.nc')
d1= pd.DataFrame()
d1=dt1.to_dataframe()
d1=d1.resample('30Min').mean()
d1.index.names=['time']
dt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultrafine2.nc')
d2= pd.DataFrame()
d2=dt2.to_dataframe()
d2=d2.resample('30Min').mean()
d2.index.names=['time']
########################################################
#######################################################
#            NUCL. Particles 325
####################################################
dxt1= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine1.nc')
dx1= pd.DataFrame()
dx1=dxt1.to_dataframe()
dx1=dx1.resample('30Min').mean()
dx1.index.names=['time']
dxt2= xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ultra325fine2.nc')
dx2= pd.DataFrame()
dx2=dxt2.to_dataframe()
dx2=dx2.resample('30Min').mean()
dx2.index.names=['time']
####################################################################################################################
# 
#
###################################################################################################################
dsx1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/reactive_new.nc')
tr = dsx1.to_dataframe()
df_tr=pd.DataFrame()
df_tr=tr.resample('30Min').mean()
df_tr.index.names=['time']
############################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/meteo_new.nc')
tir = ds1.to_dataframe()
df_tir=pd.DataFrame()
df_tir=tir.resample('30Min').mean()
df_tir.index.names=['time']
###############################################################################
ds5 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/teta.nc')
ti = ds5.to_dataframe()
df_ti=pd.DataFrame()
df_ti=ti.resample('30Min').mean()
df_ti.index.names=['time']
############## OPEN RADIATION #####################################################
#######################################
#              Build the dfg Array                     #
########################################################
date_rng = pd.date_range(start='2018-01-01 00:00:00',end='2020-10-31 23:30:00',freq='30T')
dfg =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','time'])
#######################################################################################################
#   load alll valeus on the dfg array
#####################################################################
dfg['Isoprene 80m']=voc['isoprene_3']
dfg['Monoterpenes 80m']=voc['monoterpenes_3']
dfg['NO near-surface']=dfc['NO_0.5']
dfg['NO2 - canopy height']=dfc['NO2_12']
dfg['O3 - canopy height']=df_tr['O3_12']
dfg['Sub<40 nm - 60 m']=d1['diam1']+d2['diam2']
dfg['var_12']=df_ti['Precipitation']
dfg['time']=dfg.index
################################################################
#           Select Range of Analysis
################################################################
dfg=dfg[(dfg['time'].dt.month>=2) & (dfg['time'].dt.month<=5)]

#                            RAIN and the hour before too...
dfg=dfg[dfg['var_12']>=0.5]
#
dfg=dfg.drop('var_12', axis=1)
#
#
# Compute the correlation matrix
corr = dfg.corr()

# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(12, 12))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(230, 20, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.legend(loc='upper center',title='  Rain Events -  Cross-Correlation' , fontsize=24)
ax.yaxis.label.set_size(14)
ax.xaxis.label.set_size(14)
plt.rcParams['legend.fontsize'] = 15
#plt.tight_layout()
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.9a.jpg'
plt.savefig(filename,format='jpg', dpi=300)
plt.show()



#                            NO RAIN and the hour before too...
#
#   load alll valeus on the dfg array
#####################################################################
#######################################################
date_rng = pd.date_range(start='2018-01-01 00:00:00',end='2020-10-31 23:30:00',freq='30T')
dfx =pd.DataFrame(index=date_rng, columns=['var_1','var_2','var_3','var_4','var_5','var_6','var_7','var_8','var_9','var_10','var_11','var_12','time'])
#######################################################################################################
dfx['Isoprene 80m']=voc['isoprene_3']
dfx['Monoterpenes 80m']=voc['monoterpenes_3']
dfx['NO near-surface']=dfc['NO_0.5']
dfx['NO2 - canopy height']=dfc['NO2_12']
dfx['O3 - canopy height']=df_tr['O3_12']
dfx['Sub<40 nm - 60 m']=d1['diam1']+d2['diam2']
dfx['var_12']=df_ti['Precipitation']
dfx['time']=dfx.index
#
dfx=dfx[dfx['var_12']==0]
#
dfx=dfx.drop('var_12', axis=1)
#
#
# Compute the correlation matrix
corr = dfx.corr()

# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(12, 12))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(230, 20, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.legend(loc='upper center',title='  No Rain Events  - Cross-Correlation' , fontsize=24)
ax.yaxis.label.set_size(14)
ax.xaxis.label.set_size(14)
plt.rcParams['legend.fontsize'] = 15
plt.tight_layout()
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.9b.jpg'
plt.savefig(filename,format='jpg', dpi=300)
plt.show()