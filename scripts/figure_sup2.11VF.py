import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from numpy import asarray
from numpy import savetxt
import csv
import sys
from datetime import datetime, timedelta
import matplotlib.path as mpath
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import seaborn as sns
import warnings 
import pytz
##########################################################################
##################################################################################################################33
############################################################################
ds1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/teta.nc')
tir = ds1.to_dataframe()[['Precipitation']]
df_tir=pd.DataFrame()
df_tir=tir.resample('30Min').max()
df_tir.index.names=['time']
#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
df_tir.index=df_tir.index.tz_localize('UTC')
df_tir.index=df_tir.index.tz_convert(local_timezone)
########################
df_tir=df_tir.loc['2018-01-01 00:00:00':'2020-05-31 00:00:00']
###########################################################################
ds1=xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ATTO-SMPS-clean_stp-transm-2014-202009_60m_correction_2021.nc')
ds11 = ds1.to_dataframe()
df_t = ds11.drop(['doy', 'dec.t'], axis=1)
df_temp=df_t.resample('30Min').mean()   
#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
df_temp.index=df_temp.index.tz_localize('UTC')
df_temp.index=df_temp.index.tz_convert(local_timezone)
df_temp.columns=df_temp.columns.str.replace(r'diam','')
#
df_temp=df_temp.loc['2018-01-01 00:00:00':'2020-05-31 00:00:00']
###############################################################################################################
#
df_temp.index.name = 'time'  # Define o titulo da coluna index como Date
#
####################
###################################################################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/reactive_new.nc')
dfc1=df1.to_dataframe()
#
tir= pd.DataFrame(dfc1['O3_4'])
tir['var_3']=tir['O3_4']
tir = tir.drop(['O3_4'], axis=1)
#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
tir.index=tir.index.tz_localize('UTC')
tir.index=tir.index.tz_convert(local_timezone)
#
tir=tir.loc['2018-01-01 00:00:00':'2020-05-31 00:00:00']
#########################################################################################
df_temp=df_temp[(df_temp.index.month>=2) & (df_temp.index.month<=5)] ####    WET 
tir=tir[(tir.index.month>=2) & (tir.index.month<=5)] ####    WET 
df_tir=df_tir[(df_tir.index.month>=2) & (df_tir.index.month<=5)] ####    WET 
#############################################################################################
for i in range(len(tir)):
    if(tir.index.hour[i]>=5.1):tir['var_3'][i]=np.nan
dfg=tir.resample('1440T').max()
#
tr=pd.DataFrame(dfg)
tr.index=dfg.index
tr['x']=0
for i,a in enumerate(dfg['var_3']):
#
#               FOR FIGURE S2.11A SELECT HIGH OZONE CONC.
#               FOR FIGURE S2.11b SELECT LOW OZONE CONC.
#            change as function of high AND low ozone conc.
#
    #if(a>dfg['var_3'].quantile(0.75)):tr['x'].iloc[i]=1
    if(a<dfg['var_3'].quantile(0.25)):tr['x'].iloc[i]=1
############################################################################
#
tr=tr.loc['2018-01-01 00:00:00':'2020-05-31 00:00:00']
tr=tr.resample('30Min').ffill()
tr=tr[(tr.index.month>=2) & (tr.index.month<=5)] ####    WET 
#############################################################
print(tr)
print(df_tir)
print(df_temp)
#######################################################################
for i,a in enumerate(tr['x']):
    if(a!=1 ):df_tir['Precipitation'][i]=np.nan
#
#dfavg=df_tir.groupby(df_tir.index.hour).mean()
#
#
df_count=pd.DataFrame()
df_count.index=df_tir.index
df_count['Precipitation']=np.nan
for i in range(len(df_tir)):
     if(df_tir['Precipitation'][i]> 0.5):
         df_count['Precipitation'][i]=1
     else:
         df_count['Precipitation'][i]=0
# #
dfavg=df_count.groupby(df_count.index.hour).mean()
dfavg=dfavg*100/(dfavg.sum())
print(dfavg)

#########################################################################
#
df_temp_rain=df_temp[tr['x']==1]
#
print(df_temp_rain)
#
dfh_avg_rain=df_temp_rain.groupby(df_temp_rain.index.hour).mean()
#
dfh_avg=dfh_avg_rain
print(dfh_avg)
#
fig, ax = plt.subplots(figsize=(15,6), tight_layout=True)
#
plt.margins(0,0) # Ajusta as margins da area do grafico
ax = sns.heatmap(dfh_avg.T, cmap='turbo',vmax=650,vmin=0, ax=ax, cbar_kws={"orientation": "vertical", "pad":0.07, 'aspect':9,'label': 'Number Concentration [$nm^{-1} cm^{-3}$]'},annot_kws={"size": 30}, xticklabels=1, yticklabels=8)
fig.autofmt_xdate() # Inclina o texto contendo a data no eixo X, isso ajuda a visualizar melhor a data no eixo X
#
ax.invert_yaxis()

label = ['10', '40', '70', '100', '130', '160', '190', '220', '250', '280','310','340','370']
ax.set_yticklabels(label,fontsize=14,rotation='horizontal')
ax.yaxis.tick_right()
#
ax.set_ylabel('Particle Size (nm)',fontsize=13)
ax.set_xlabel('Local Time ',fontsize=13)
ax.tick_params(axis='both', which='major', labelsize=13)
ax.figure.axes[-1].yaxis.label.set_size(13)
#
###########################################################
#
#               FOR FIGURE S2.11A SELECT HIGH OZONE CONC.
#               FOR FIGURE S2.11b SELECT LOW OZONE CONC.
#            change as function of high AND low ozone conc.
#
#plt.figtext(.55,.92,'Nights - High Ozone Conc. ',color='white',fontsize='x-large',fontweight='bold')
plt.figtext(.55,.92,'Nights - Low Ozone Conc. ',color='white',fontsize='x-large',fontweight='bold')
#
#
#####################################################################################
ax2=plt.twinx()
plt.margins(0,0) # Ajusta as margins da area do grafico
#
dfavg.index=dfavg.index+0.5 
dfavg.plot(y="Precipitation", ax=ax2, legend=False, lw=2,color="black")
#
ax2.set_ylabel('rainning events (%)',fontsize=15)
ax2.tick_params(axis='both', which='major', labelsize=15)
ax2.set_ylim(0,11.0)
#######################################################################################
plt.tight_layout()
#
#               FOR FIGURE S2.11A SELECT HIGH OZONE CONC.
#               FOR FIGURE S2.11b SELECT LOW OZONE CONC.
#            change as function of high AND low ozone conc.
#
#plt.savefig('/home/machado/work/gases/Gitlab/xfigure_vf_npf/figure_sup211a.jpg', bbox_inches='tight', pad_inches=0.2,dpi=300)
plt.savefig('/home/machado/work/gases/Gitlab/xfigure_vf_npf/figure_sup2.11b.jpg', bbox_inches='tight', pad_inches=0.2,dpi=300)
#
plt.show()
##########################################################################################

