import xarray as xr
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools
from datetime import datetime, timedelta
import matplotlib.path as mpath
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter,AutoMinorLocator
import matplotlib as mpl
# #############################################################
# #
# #                              
# ##############################################################
color1='green'
color2='yellow'
color3='red'
color4='blue'
color5 = 'black'
#################################################################
df1 = xr.open_dataset('/home/machado/work/gases/Gitlab/data_composite/dmax_roli_wet_var_12.nc')
df_glm_q25= pd.DataFrame()
df_glm_q25=df1.to_dataframe()
##################################################################################
#############################################################################################
print(df_glm_q25)
dff=df_glm_q25
dff.index=df_glm_q25['tstep']
ix=0
ixx=0
for i in range(len(dff)):
    if(dff.index[i]==-2): 
        ixx=ixx+1
for i in range(len(dff)):
    if(dff.index[i]==-2 and math.isnan(dff['var_8'].iloc[i])): 
        ix=ix+1
print('total values geral =  ',ixx)
print('total values on composite=  ',ixx-ix)
#
################################################################################
##
#                               Part
######################################################################################################
stix=[]
for i,ia in enumerate(df_glm_q25['case']):
    stix.append(i)
df_glm_q25.index=stix
########################################################################################
#
fig,ax=plt.subplots(figsize=(10,8))

ax.spines['left'].set_color('black')
ax.tick_params(axis='both', which='major', labelsize=15)

ax.spines['left'].set_linewidth(3)
ax.spines['bottom'].set_linewidth(3)
ax.spines['top'].set_linewidth(3)
ax.spines['right'].set_linewidth(3)
#
filename='/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.2.jpg'
#
ax.yaxis.set_minor_locator(  AutoMinorLocator(5))
ax.xaxis.set_minor_locator(  AutoMinorLocator(5))
#
plot1=sns.lineplot(x='tstep',y='var_8',data=df_glm_q25,ci=95,ax=ax,color=color4,lw=2, marker=('o'),label='Conc<40 nm at 0-80m ',legend=False, estimator=np.mean)
plot1=sns.lineplot(x='tstep',y='var_9',data=df_glm_q25,ci=95,ax=ax,color=color3,lw=2, marker=('*'),label='Conc<40 nm at 240-280m . ',legend=False, estimator=np.mean)
#
h1, l1 = ax.get_legend_handles_labels()
#
plt.legend(h1, l1, loc=2)
plt.title('ROLI May 2023')
ax.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
#
#
sbox_to_anchor=(1, 0.5)
#
ax.set_xlabel("Hours Before and After Maximum Rain Rate",fontsize=15)
ax.set_ylabel(" Dif. Conc. (0-80)m - Conc.(240-320)m - Num. Con.  [$cm^{-3}$] ",fontsize=15)
ax.tick_params(axis='both', which='major', labelsize=15)

#
plt.tight_layout()
plt.savefig(filename,format='jpg', dpi=300)
plt.show()
#
