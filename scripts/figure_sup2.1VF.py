import xarray as xr
import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns 
import pytz
from matplotlib.ticker import MaxNLocator
from matplotlib.ticker import (MultipleLocator,
                               FormatStrFormatter,
                               AutoMinorLocator)
#
############################################################################
ds2 = xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/teta.nc')
tir = ds2.to_dataframe()[['Precipitation']]
##############################################################################
df_tir=pd.DataFrame()
df_tir=tir.resample('5Min').sum()
#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
df_tir.index=df_tir.index.tz_localize('UTC')
df_tir.index=df_tir.index.tz_convert(local_timezone)
#
ds1=xr.open_dataset('/home/machado/work/gases/Gitlab/dados_fonte/ATTO-SMPS-clean_stp-transm-2014-202009_60m_correction_2021.nc')
ds11 = ds1.to_dataframe()
df_t = ds11.drop(['doy', 'dec.t'], axis=1)
df_temp=df_t.resample('5Min').mean()   
#
#            Convert from UTC to Local Time
#
local_timezone = pytz.timezone("America/Manaus")
df_temp.index=df_temp.index.tz_localize('UTC')
df_temp.index=df_temp.index.tz_convert(local_timezone)
df_temp.columns=df_temp.columns.str.replace(r'diam','')
###############################################################################################################
#
df_temp.index.name = 'Date'  # Define o titulo da coluna index como Date
df_tir.index.name = 'Date'   # Define o titulo da coluna index como Date
#
##########################################################################################
#
#########################################################################
df_tir=df_tir.loc['2019-03-29 20:00:00':'2019-03-30 10:00:00']
df_temp=df_temp.loc['2019-03-29 20:00:00':'2019-03-30 10:00:00']
df_tir=df_tir[df_tir>=1.0]
#########################################################################
#
df_temp.index = df_temp.index.strftime('%Y-%m-%d %H:%M') # Formata a data que será utilizada no eixo X
#
fig, ax = plt.subplots(figsize=(10,5), tight_layout=True)
#
plt.margins(0,0) # Ajusta as margins da area do grafico
ax = sns.heatmap(df_temp.T, cmap='viridis',vmin=0,vmax=300, ax=ax, cbar_kws={"orientation": "vertical", "pad":0.07, 'aspect':9,'label': 'Number Concentration [$nm^{-1} cm^{-3}$]'},annot_kws={"size": 30}, xticklabels=28, yticklabels=4)
fig.autofmt_xdate() # Inclina o texto contendo a data no eixo X, isso ajuda a visualizar melhor a data no eixo X
#
ax.invert_yaxis()
#
ax.figure.axes[-1].yaxis.label.set_size(15)
#
ax2=plt.twinx()
plt.margins(0,0) # Ajusta as margins da area do grafico
#
ax2.scatter(range(df_tir.index.values.shape[0]), df_tir["Precipitation"], color="red", s=40)
ax2.scatter(range(df_tir.index.values.shape[0]), df_tir["Precipitation"], color="red", s=15)
#
#
ax.set_ylim([0,65])
ax.set_ylabel('Particle Size (nm)',fontsize=15)
ax.set_xlabel('Local Time',fontsize=15)
ax2.set_ylabel('Rain Rate (mm.hour$^{-1}$)',fontsize=15)
ax.tick_params(axis='both', which='major', labelsize=13)
ax2.tick_params(axis='both', which='major', labelsize=15)

#
plt.tight_layout()
plt.savefig('/home/machado/work/gases/Gitlab/xfigure_vf_npf/fig_sup2.1.jpg', bbox_inches='tight', pad_inches=0,dpi=300)
plt.show()
#
